﻿Dobra, rozpisze się co działa, a co nie:
- SIM808
+ uno działa jeśli chodzi o wysyłanie podstawowych danych mqtt. Proste rzeczy jak prąd, prędkość i debug wysyła.
+ działa stabilnie, nie gaśnie po 10 minutach wysyłania, wysyła dane w krótszych odstępach czasu.
+ Teraz co nie działa: nie pobiera danych z GPSu, ani pozycji ani czasu. Nie wiem czy to problem z kodem, czy z anteną, nie doszedłem do tego jeszcze.
+ zajmuje 70% pamięci na Uno, z Megą ciągle nie działa.

- SIM900:
+ działa wysyłanie danych jak wyżej, 
+ sam z siebie się wyłączył za każdym razem po ok. 1min. Prawdopodobnie jest to związane z jakimś oszczędzaniem energii zaimplementowanym w modemie. Jeśli tak, na 90% da się to wyłączyć.
+ wysyłanie trwa dłużej niż na SIM808
+ uruchamia się dłużej, ale da się to poprawić jedna komendą.
+ NIE jestem w stanie pobrać tutaj czasu z sieci - wywala Error.