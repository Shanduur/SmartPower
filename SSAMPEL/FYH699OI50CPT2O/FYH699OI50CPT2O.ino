#include <SoftwareSerial.h>
#include <MqttSerial.h>
#include <JsonParser.h>


SoftwareSerial bluetoothSerial(10,11);
MqttSerial mqttSerial ("d:quickstart:<HTC One>:<980d2e432b81>",bluetoothSerial);
// type-id:	is an identifier you provide, for example “acme-thing”. (If you have a number of similar devices; 
// perhaps all those running the same code, you should use the same type-id for all of them) 

// device-id: device-id is a 12 hexadecimal character MAC address in lower case, without delimiting characters. For example, a36d7c91bf9e. 

void callback(String, String) ;

void setup() {
  Serial.begin(9600);
  Serial.println("Serial initialized");
  bluetoothSerial.begin(9600);
  pinMode(13, OUTPUT);
  mqttSerial.begin("quickstart.messaging.internetofthings.ibmcloud.com", "1883", "", "");  
  mqttSerial.connect();
 }
void callback(String topic, String payload) {
   Serial.println(topic + " : " + payload);
}
void loop() {
	mqttSerial.loop(callback);
	int currVal = analogRead(A0);
 
    String v(currVal);
    String pub = "{\"d\": {\"Temp\": \""+ v +"\"}}";
    mqttSerial.publish("iot-2/evt/status/fmt/json", pub);
	delay(200);  
}
