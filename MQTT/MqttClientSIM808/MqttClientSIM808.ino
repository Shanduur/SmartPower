/*
* Subskrypcja wiadomosci w mosquitto-clients (sudo apt-get install mosquitto-clients:
*
* Debug:
*   mosquitto_sub -h test.mosquitto.org -t SmartPower/debug -q -l
*
* Dane telemetryczne:
*
mosquitto_sub -h test.mosquitto.org -t SmartPower/prad -t SmartPower/temperatura
-t SmartPower/okrazenie -t SmartPower/czasOkrazenia -t SmartPower/GPS -q -l
*
*/

#define SerialMon Serial
/*
* Ustawienie serialu dla debugu w konsoli (dla Serial Monitora, bazowa predkosc to 115200)
*
*/

//#define SerialAT Serial1
/*
*  Hardware Serial uzywany na Mega, Leonardo, Micro
*  Gdy modul ma zworki (SIM900 IComSat v1.1 bez GPS)
*  NIE DZIALA z WaveShare GSM/GPRS/GPS Shield (B) SIM808
*
*/

#include <SoftwareSerial.h>
SoftwareSerial SerialAT(2, 3); // RX, TX
/*
*  Softwarowy Serial
*  Dla modulu WaveShare GSM/GPRS/GPS Shield (B) SIM808
*  Predkosc modulu to 115200
*
*/

//#define TINY_GSM_MODEM_SIM900
#define TINY_GSM_MODEM_SIM808
/*
* Definicja modulu ktory jest aktualnie uzywany
* Przy zmianie konieczna jest zmiana w bibliotece TinyGsmClientSIM800.h
* Dopisanie lub usuniecie int w linijce 761
* Poprawna dla SIM808: rsp = waitResponse(75000L,
* Poprawna dla SIM900: int rsp = waitResponse(75000L,
*
*/

#include <stdlib.h>

#include <TinyGsmClient.h>
/*
* Biblioteka zawierajaca obsluge MQTT
*
*/
#include <PubSubClient.h>
/*
* Publikacja i Subskrypcja
*
*/

//#include "GetGPS.h"
/*
* Wywala bledy, do poprawki
* Definicje funkcji zwiazanych z GPSem
*
*/

#include <DFRobot_sim808.h>
/*
* Biblioteka DFRobot dla SIM808
* Zawiera obsluge GPS
*
*/
DFRobot_SIM808 sim808(&SerialAT);

int onModulePin = 1;

//--------------------------------------------------------------------------------------------------------------
// *************************************************** MQTT ****************************************************
//--------------------------------------------------------------------------------------------------------------

const char apn[] = "internet"; //T-Mobile
const char user[] = "";
const char pass[] = "";
/*
* Dane APN karty SIM
* Pozostawić puste, jesli user albo password sa nieuzywane
*
*/

// ******* Detale MQTT *******
const char* broker = "test.mosquitto.org";
/*
* Adres brokera MQTT
* test.mosquitto.org jest publiczny
*
*/
const char* topicDebug = "SmartPower/debug";

// ******* Definicje topiców z poszczególnymi parametrami *******
const char* topicPrad = "SmartPower/prad";
const char* topicTemperatura = "SmartPower/temperatura";
const char* topicOkrazenie = "SmartPower/okrazenie";
const char* topicCzasOkrazenia = "SmartPower/czasOkrazenia";
const char* topicSzerokosc = "SmartPower/Szerokosc";
const char* topicDlugosc = "SmartPower/Dlugosc";
const char* topicPredkosc = "SmartPower/Predkosc";

// ******* Definicje zmiennych poszczególnych parametrów *******
double prad = 1.0;
char c_prad[8];

double temperatura = 2.0;
char c_temperatura[8];

char c_predkosc[8];

int okrazenie = 3;
char c_okrazenie[8];


// ******* GPS *******
struct s_szerokosc
{
  char stopnie[4];
  char minuty[4];
  char sekundy[8];
}szerokosc;

struct s_dlugosc
{
  char stopnie[4];
  char minuty[4];
  char sekundy[8];
}dlugosc;

struct s_data
{
  char* rok;
  char* miesiac;
  char* dzien;
  char* godzina;
  char* minuta;
  char* sekunda;
  char* milisek;
}data;

/*
* Wartosci przypisane do testow
*
*/

int modemGPS_licznik = 1;

TinyGsm modem(SerialAT);
TinyGsmClient client(modem);
PubSubClient mqtt(client);

long lastReconnectAttempt = 0;
//--------------------------------------------------------------------------------------------------------------



//--------------------------------------------------------------------------------------------------------------
// ************************************************** SETUP ****************************************************
//--------------------------------------------------------------------------------------------------------------
void setup()
{
  // Ustawienie predkosci konsoli
  SerialMon.begin(9600);
  delay(100);

  // ********************** MODEM **********************
  SerialAT.begin(9600);
  if (SerialAT.available())
  {
    SerialMon.println("OK");
  }
  delay(100);
  /*
  * Ustawienie predkosci modulu GSM
  * Sprawdzanie domyslnej za pomoca sketchu AT_Debug
  * Dla modułu SIM900 IComSat 1.1 predkosc to 9600
  *
  */

  // Restart trwa dosyc długo
  // Aby pominac, wywolaj init() zamiast restart()
  //power_on();
  SerialMon.println("Inicjalizuje Modem...");
  modem.init();

  String modemInfo = modem.getModemInfo();
  SerialMon.print("Modem: ");
  SerialMon.println(modemInfo);

  // Odblokuj karte SIM za pomoca kodu PIN jesli zablokowana
  //modem.simUnlock("1234");

  SerialMon.print("Oczekiwanie na siec...");
  if (!modem.waitForNetwork())
  {
    SerialMon.println(" blad");
    while (true);
  }
  SerialMon.println(" OK");

  SerialMon.print("Laczenie do ");
  SerialMon.print(apn);

  if (!modem.gprsConnect(apn, user, pass))
  {
    SerialMon.println(" blad");
    while (true);
  }
  SerialMon.println(" OK");

  // MQTT Broker setup
  mqtt.setServer(broker, 1883);
  //mqtt.setCallback(mqttCallback);
}

boolean mqttConnect()
{
  SerialMon.print("Laczenie z ");
  SerialMon.print(broker);
  if (!mqtt.connect("SmartPower MQTT Mosquitto test"))
  {
    SerialMon.println(" blad");
    return false;
  }
  SerialMon.println(" OK");
  mqtt.publish(topicDebug, "modul telemetri SmartPower uruchomiony");
  return mqtt.connected();
}
//--------------------------------------------------------------------------------------------------------------



//--------------------------------------------------------------------------------------------------------------
// *************************************************** LOOP ****************************************************
//--------------------------------------------------------------------------------------------------------------
void loop()
{

  if (mqtt.connected())
  {
    SP_mqttSending();
  }
  else
  {
    // Reconnect every 10 seconds
    unsigned long t = millis();
    if (t - lastReconnectAttempt > 10000L)
    {
      lastReconnectAttempt = t;
      if (mqttConnect())
      {
        lastReconnectAttempt = 0;
      }
    }
  }
}
//--------------------------------------------------------------------------------------------------------------



//--------------------------------------------------------------------------------------------------------------
// *************************************************** MQTT ****************************************************
//--------------------------------------------------------------------------------------------------------------

void publish_mqtt()
{
  mqtt.publish(topicPrad, c_prad);  delay(100);

  mqtt.publish(topicPredkosc, c_predkosc);  delay(100);

  mqtt.publish(topicTemperatura, c_temperatura);  delay(100);

  mqtt.publish(topicOkrazenie, c_okrazenie);  delay(100);

  mqtt.publish(topicCzasOkrazenia, data.rok);     delay(100);
  mqtt.publish(topicCzasOkrazenia, data.miesiac); delay(100);
  mqtt.publish(topicCzasOkrazenia, data.dzien);   delay(100);
  mqtt.publish(topicCzasOkrazenia, data.godzina); delay(100);
  mqtt.publish(topicCzasOkrazenia, data.minuta);  delay(100);
  mqtt.publish(topicCzasOkrazenia, data.sekunda); delay(100);
  mqtt.publish(topicCzasOkrazenia, data.milisek); delay(100);

  mqtt.publish(topicSzerokosc, szerokosc.stopnie);  delay(100);
  mqtt.publish(topicSzerokosc, szerokosc.minuty);   delay(100);
  mqtt.publish(topicSzerokosc, szerokosc.sekundy);  delay(100);

  mqtt.publish(topicDlugosc, dlugosc.stopnie);  delay(100);
  mqtt.publish(topicDlugosc, dlugosc.minuty);   delay(100);
  mqtt.publish(topicDlugosc, dlugosc.sekundy);  delay(100);
}

void SP_mqttSending()
{
  dtostrf(temperatura, 2, 2, c_temperatura);
  dtostrf(prad, 2, 2, c_prad);

  GPS_meth();

  publish_mqtt();

  SerialMon.print(" Wyslano zestaw danych:");
  SerialMon.print(modemGPS_licznik);        delay(100);
  modemGPS_licznik = modemGPS_licznik + 1;
}
//--------------------------------------------------------------------------------------------------------------

void GPS_meth()
{
  if (sim808.getGPS()) 
    {
      data.rok = (char*)sim808.GPSdata.year;             //uint16_t
      data.miesiac = (char*)sim808.GPSdata.month;        //uint8_t
      data.dzien = (char*)sim808.GPSdata.day;            //uint8_t
      data.godzina = (char*)sim808.GPSdata.hour;         //uint8_t
      data.minuta = (char*)sim808.GPSdata.minute;        //uint8_t
      data.sekunda = (char*)sim808.GPSdata.second;     	 //uint8_t
      data.milisek = (char*)sim808.GPSdata.centisecond;  //uint8_t
    
      sim808.latitudeConverToDMS();
      //"latitude :"
      itoa(sim808.latDMS.degrees,szerokosc.stopnie,2);      //int
      itoa(sim808.latDMS.minutes,szerokosc.minuty,2);       //int
      sprintf(szerokosc.sekundy,"%1.6f",sim808.latDMS.seconeds);  //float

      sim808.LongitudeConverToDMS();
      //"longitude :"
      itoa(sim808.longDMS.degrees,dlugosc.sekundy,2);       //int
      itoa(sim808.longDMS.minutes,dlugosc.sekundy,2);       //int
      sprintf(dlugosc.sekundy,"%1.6f",sim808.longDMS.seconeds);   //float
    
      //"speed_kph :"
      sprintf(c_predkosc,"%1.6f",sim808.GPSdata.speed_kph); //float

      //************* wylacz modul GPS ************
      sim808.detachGPS();
    }
}

//--------------------------------------------------------------------------------------------------------------

void power_on()
{

    uint8_t answer=0;

    digitalWrite(onModulePin,HIGH);
    delay(3000);
    digitalWrite(onModulePin,LOW);
}
