
#include <SPI.h>
#include <MCP2515.h>
#include "Can_ID.h"

#define CAN_CLK 8
#define CAN_BAUD 250

#define CAN_SPI_MCP_PIN	 4
#define CAN_INTERRUPT_PIN 2
#define CAN_INTERRUPT_NUMBER 0
#define CAN_RTR 1
#define CAN_DATA 0
#define TEMP_TIME 100
//****************************prototypy funckji*************************//
void CAN_Interrupt(void); //przerwanie wywoływane przy przyjściu wiadomości
void CAN_InitStageOne(void); //pierwszy etap inicjalizacji CAN
void SPI_Init(void); //inicjalizacja SPI na potrzeby CAN


//MCP_CAN CAN(CAN_SPI_MCP_PIN);

MCP2515 CAN(CAN_SPI_MCP_PIN, CAN_INTERRUPT_PIN);
Frame message;

void setup()
{

	Serial.begin(115200);
	Serial.println("Rozpoczynam inicjalizacje");

	Serial.println("Inicjalizuje CAN");
	CAN_InitStageOne();

	//Serial.println("Inicjalizuje Przerwania");
	//attachInterrupt(CAN_INTERRUPT, CAN_Interrupt, FALLING); // start interrupt // wlaczenie przrwan od modulu CAN

	Serial.println("Zakonczenie inicjalizacji");
	Serial.println("wysyłanie ramek kontrolnych");
	uint8_t test[8];
	test[0] = 1;
	test[1] = 2;
	test[2] = 3;
	//Serial.println("EXTENDED ID's");
	//CAN.sendMsgBuf(1301, CAN_STDID, CAN_DATA, 3, test);
	//delay(100);
	//Serial.println("STANDARD ID's");
	//CAN.sendMsgBuf(1302, CAN_STDID,CAN_DATA, 3, test);
	//delay(100);
	//Serial.println("REMOTEFRAME");
	//CAN.sendMsgBuf(1303, CAN_STDID, CAN_RTR, 0, test);
	delay(100);

	Serial.println("==================================================="); 


}
unsigned long previousMillis = millis();
void loop()
{
	delay(10);
	unsigned long currentMillis = millis();
	if(currentMillis - previousMillis > TEMP_TIME) {
    // save the last time you blinked the LED 
		previousMillis = currentMillis;   
		
		delay(1000);
		message.rtr = CAN_DATA;
		message.id = CAN_ID_CAR_VELOCITY;
		message.data.byte[0]=2;
		message.data.byte[1]=4;
		//CAN.LoadBuffer(buffer, &message);
		Serial.println(message.id);
		CAN.EnqueueTX(message);
		delay(100);
	}
}


void CAN_Interrupt(void)
{
	CAN.intHandler();
	//byte len = 0;
	//byte buf[8];
	//unsigned long ID = 0;
	//uint8_t ext = 0;
	//uint8_t rtr = 0;

	//CAN.readMsgBufID(&ID, &len, buf);
	//rtr = CAN.isRemoteRequest();
	//ext = CAN.isExtendedFrame();

	if (CAN.GetRXFrame(message)) 
	{
		Serial.print("ID = ");
		Serial.print(message.id);

		Serial.print("\t");
		if (message.extended)
		{
			Serial.print("EXTENDED");
		}
		else
		{
			Serial.print("STANDARD");
		}

		Serial.print("\t");
		if (message.rtr)
		{
			Serial.println("REMOTE");
		}
		else
		{
			Serial.print("DATA");
			Serial.print("\t len:");
			Serial.println(message.length);
			for (int i = 0; i < message.length; i++)
			{
				Serial.print(message.data.byte[i], DEC);
				Serial.print("\t");
			}
			Serial.println();
		}
		Serial.println("===================================================");
		//CAN.EnqueueTX(message);
	}
}


void CAN_InitStageOne(void)
{
	//int count = 50;                                     // the max numbers of initializint the CAN-BUS, if initialize failed first!.  
	//do {
	//	if (CAN_OK == CAN.begin(CAN_500KBPS, MCP_16MHz))             // init can bus : baudrate = 500k
	//	{
	//		Serial.println("CAN_InitStageOne - inicjalziacja powiodla sie");
	//		break;
	//	}
	//	else
	//	{
	//		Serial.println("CAN_InitStageOne - inicjalizacja modulu CAN nie powiodla sie");
	//		Serial.println("CAN_InitStageOne - ponowna proba inicjalizacji");
	//		delay(10);
	//		if (count <= 1)
	//		{
	//			Serial.println("CAN_InitStageOne - inicjalizacja modulu nie powiodla sie");
	//		}
	//		
	//	}

	//} while (count--);

	SPI_Init();
	if (CAN.Init(CAN_BAUD, CAN_CLK))
	{
		Serial.println("MCP2515 Init OK ...");
	}
	else {
		Serial.println("MCP2515 Init Failed ...");
	}

	attachInterrupt(CAN_INTERRUPT_NUMBER, CAN_Interrupt, FALLING);
	CAN.InitFilters(false);
	CAN.SetRXMask(MASK0, 0x0, 0); //match all but bottom four bits
	CAN.SetRXFilter(FILTER0, 0x100, 0); //allows 0x100 - 0x10F
										//So, this code will only accept frames with ID of 0x100 - 0x10F. All other frames
										//will be ignored.
}


void SPI_Init(void)
{
	// Set up SPI Communication
	// dataMode can be SPI_MODE0 or SPI_MODE3 only for MCP2515
	SPI.setClockDivider(SPI_CLOCK_DIV2);
	SPI.setDataMode(SPI_MODE0);
	SPI.setBitOrder(MSBFIRST);
	SPI.begin();

}


/*
MCP2515 CAN Interface Using SPI

Author: David Harding

Created: 11/08/2010
Modified: 6/26/12 by RechargeCar Inc.

For further information see:

http://ww1.microchip.com/downloads/en/DeviceDoc/21801e.pdf
http://en.wikipedia.org/wiki/CAN_bus

The MCP2515 Library files also contain important information.

This sketch is configured to work with the 'Macchina' Automotive Interface board
manufactured by RechargeCar Inc. CS_PIN and INT_PIN are specific to this board.

This sketch shows the most basic of steps to send and receive CAN messages.

NOTE!!!  If you use this sketch to test on a live system I suggest that you comment out the
send messages lines unless you are certain that they will have no detrimental effect!


This example code is in the public domain.

*/
//
//#include <SPI.h>
//#include <MCP2515.h>
//
//// Pin definitions specific to how the MCP2515 is wired up.
//#define CS_PIN    10
//#define INT_PIN    2
//
//// Create CAN object with pins as defined
//MCP2515 CAN(CS_PIN, INT_PIN);
//
//void CANHandler() {
//	Serial.println("przerwaniue");
//	CAN.intHandler();
//
//}
//
//void setup() {
//	Serial.begin(115200);
//
//	Serial.println("Initializing ...");
//
//	// Set up SPI Communication
//	// dataMode can be SPI_MODE0 or SPI_MODE3 only for MCP2515
//	SPI.setClockDivider(SPI_CLOCK_DIV2);
//	SPI.setDataMode(SPI_MODE0);
//	SPI.setBitOrder(MSBFIRST);
//	SPI.begin();
//
//	// Initialize MCP2515 CAN controller at the specified speed and clock frequency
//	// (Note:  This is the oscillator attached to the MCP2515, not the Arduino oscillator)
//	//speed in KHz, clock in MHz
//	if (CAN.Init(250, 16))
//	{
//		Serial.println("MCP2515 Init OK ...");
//	}
//	else {
//		Serial.println("MCP2515 Init Failed ...");
//	}
//
//	attachInterrupt(0, CANHandler, FALLING);
//	CAN.InitFilters(false);
//	CAN.SetRXMask(MASK0, 0x0, 0); //match all but bottom four bits
//	CAN.SetRXFilter(FILTER0, 0x100, 0); //allows 0x100 - 0x10F
//										//So, this code will only accept frames with ID of 0x100 - 0x10F. All other frames
//										//will be ignored.
//
//	Serial.println("Ready ...");
//}
//
//byte i = 0;
//
//// CAN message frame (actually just the parts that are exposed by the MCP2515 RX/TX buffers)
//Frame message;
//
//void loop() {
//
//	if (CAN.GetRXFrame(message)) {
//		// Print message
//		Serial.print("ID: ");
//		Serial.println(message.id, DEC);
//		Serial.print("Extended: ");
//		if (message.extended) {
//			Serial.println("Yes");
//		}
//		else {
//			Serial.println("No");
//		}
//		Serial.print("Length: ");
//		Serial.println(message.length, DEC);
//		Serial.print("Dane: ");
//		for (i = 0; i<message.length; i++) {
//			Serial.print(message.data.byte[i], DEC);
//			Serial.print("\t");
//		}
//		Serial.println();
//		Serial.println();
//
//		// Send out a return message for each one received
//		// Simply increment message id and data bytes to show proper transmission
//		// Note: this will double the traffic on the network (provided it passes the filter above)
//		//message.id++;
//		//for (i = 0; i<message.length; i++) {
//		//	message.data.byte[i]++;
//		//}
//		//CAN.EnqueueTX(message);
//	}
//}
//
