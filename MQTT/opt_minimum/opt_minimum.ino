//////////////////////////////// UWAGI ///////////////////////////////////////
//Nalezy zastanowic sie nad przeniesiem scalania danych odebranych w *dane wysylane gdzies indziej niz funkcja przerwania
//////////////////////////////////////////////////////////////////////////////

#define ACCEPT 1
#define DENY 0

#define DEBUG 1

/*
  Subskrypcja wiadomosci w mosquitto-clients (sudo apt-get install mosquitto-clients:
  Debug:
    mosquitto_sub -h test.mosquitto.org -t SmartPower/debug -q -l
  *dane telemetryczne:
  mosquitto_sub -h test.mosquitto.org -t SmartPower/prad -t SmartPower/temperatura
  -t SmartPower/okrazenie -t SmartPower/czasOkrazenia -t SmartPower/GPS -q -l
*/

#define SerialMon Serial
/*
  Ustawienie serialu dla debugu w konsoli (dla Serial Monitora, bazowa predkosc to 9600)
  W finalnej wersji nieuzywany
*/

//#define SerialAT Serial1
/*
   Hardware Serial uzywany na Mega, Leonardo, Micro
   Gdy modul ma zworki (SIM900 IComSat v1.1 bez GPS)
   TEORETYCZNIE na WaveShare GSM/GPRS/GPS Shield (B) SIM808 powinno dzialac,
   ALE modul zaslania Serial1 :v
*/

#include <SoftwareSerial.h>
SoftwareSerial SerialAT(2, 3); // RX, TX
/*
   Softwarowy Serial
   Dla modulu WaveShare GSM/GPRS/GPS Shield (B) SIM808
   W sensie, ze nie dziala...
*/

#define TINY_GSM_MODEM_SIM808
/*
  Definicja modulu ktory jest aktualnie uzywany
  Przy zmianie konieczna jest zmiana w bibliotece TinyGsmClientSIM800.h
  Dopisanie lub usuniecie int w linijce 761
  Poprawna dla SIM808: rsp = waitResponse(75000L,
  Poprawna dla SIM900: int rsp = waitResponse(75000L,
*/

#include <stdlib.h>

#include <TinyGsmClient.h>
/*
  Biblioteka zawierajaca obsluge MQTT
*/
#include <PubSubClient.h>
/*
  Publikacja i Subskrypcja
*/

int onModulePin = 9;

//do ustalenia czasy co jakie ma byc wysylane zadanie podania aktualnych danych
//chyba ze w jakis inny sposob bedzie to zrobione (np.reakcja na akcje z aplikacji z panelu)
#define TEMP_TIME 10000
#define TIME_TIME 10000
/*#define TEMP_TIME_ENABLE 1000
  #define PREDKOSC_TIME_ENABLE 1000
  #define NAPIECIE_TIME_ENABLE 1000
  #define PRAD_TIME_ENABLE 1000*/
#define ENABLE_TIME 100
#include <SPI.h>
#include <MCP2515.h>
#include "Can_ID.h"
#define CAN_CLK 8 //kwarc
//#define CAN_BAUD 250 //predkosc CAN
#define CAN_SPI_MCP_PIN  4 //pin podlaczenia interfejsu spi
#define CAN_INTERRUPT_PIN 2 //podlaczenie interupta 
#define CAN_INTERRUPT_NUMBER 0 //numer interupta
#define CAN_RTR 1 //czy ramka z danymi czy remote
#define CAN_DATA 0
MCP2515 CAN(CAN_SPI_MCP_PIN, CAN_INTERRUPT_PIN);
Frame message;

//****************************prototypy funckji*************************//
void CAN_Interrupt(void); //przerwanie wywo³ywane przy przyjœciu wiadomoœci
void CAN_InitStageOne(void); //pierwszy etap inicjalizacji CAN
void SPI_Init(void); //inicjalizacja SPI na potrzeby CAN
/////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------------------------------------
// ************************************************** GPS ******************************************************
//--------------------------------------------------------------------------------------------------------------
char frame[200];
char latitude[15];
char longitude[15];
char altitude[6];
//--------------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------------
// *************************************************** MQTT ****************************************************
//--------------------------------------------------------------------------------------------------------------

const char apn[] = "Internet"; //T-Mobile
const char user[] = "";
const char pass[] = "";
/*
  *dane APN karty SIM
  Pozostawić puste, jesli user albo password sa nieuzywane
*/

// ******* Detale MQTT *******
const char* broker = "test.mosquitto.org";
/*
  Adres brokera MQTT
  test.mosquitto.org jest publiczny
*/
const char* topicDebug = "SmartPower/debug";

// ******* Definicje topiców z poszczególnymi parametrami *******
const char* topicSmartPower = "SmartPower/dane";
/*const char* topicPrad           = "SmartPower/prad";
  const char* topicPredkosc       = "SmartPower/predkosc";
  const char* topicNapiecie		= "SmartPower/napiecie";
  const char* topicTemperatura    = "SmartPower/temperatura";
  const char* topicOkrazenie      = "SmartPower/okrazenie";
  const char* topicCzasOkrazenia  = "SmartPower/czasOkrazenia";*/

char *dane[9];
// ******* Definicje zmiennych poszczególnych parametrów *******
/*double prad = 0.0;
  char c_prad[8];
  //zmienne do odebrania
  int prad_calk = 0;
  int prad_dze = 0;
  /*uint8_t currentTime_time = millis();
  uint8_t currentTime_temp = millis();
*/
/*//pobranie czasu poczatkowego do wysylania zadania aktualizacji temperatury
  double temperatura = 30.0;
  char c_temperatura[8];
  //zmienne do odebrania
  uint8_t temp_calk = 0;
  uint8_t temp_dze = 0;
  char c_napiecie[8];
  double napiecie = 0.0;
  //zmienne do odebrania
  uint8_t nap_calk = 0;
  uint8_t nap_dze = 0;
  double predkosc;
  char c_predkosc[8];
  //zmienne do odebrania
  uint8_t pred_calk = 0;
  uint8_t pred_dze = 0;
  uint8_t okrazenie = 0;
  char c_okrazenie[8];
  struct czas
  {
  int mnt;
  int sek;
  };
  czas czas_start;
  czas czas_okrazenia;
  /*Flagi przyjmowania danych po CAN-ie */
/*bool flagaNapiecie = ACCEPT;
  bool flagaPrad = ACCEPT;
  bool flagaPredkosc = ACCEPT;
  bool flagaTemp = ACCEPT;
  bool flagaCzas = ACCEPT;
  /*Czasy biezace zamykania sie na *dane z CANA*/
/*unsigned long currentTime_pradEnabled = 0;
  unsigned long currentTime_napiecieEnabled = 0;
  unsigned long currentTime_predkoscEnabled = 0;
  unsigned long currentTime_tempEnabled = 0;
*/
unsigned long currentTime_Enabled = 0;
/*
  Wartosci przypisane do testow
*/

int modemGPS_licznik = 1;
char c_licznik[5];

long lastReconnectAttempt = 0;
//--------------------------------------------------------------------------------------------------------------

TinyGsm modem(SerialAT);
TinyGsmClient client(modem);
PubSubClient mqtt(client);

//funckja do skladania zmiennych odebranych w calosc do wyslania
/*float assemble(int cz_calk, int cz_dze)
  {
	float liczba = (float)(cz_dze / 100);
	liczba = liczba + cz_calk;
	return liczba;
  }*/

//--------------------------------------------------------------------------------------------------------------
// ************************************************** SETUP ****************************************************
//--------------------------------------------------------------------------------------------------------------
void setup()
{
  // Ustawienie predkosci konsoli
  delay(100);

  // ********************** MODEM **********************
  SerialAT.begin(9600);
  SerialMon.begin(9600);

  delay(100);
  /*
    Ustawienie predkosci modulu GSM
    Sprawdzanie domyslnej za pomoca sketchu AT_Debug
    Dla modułu SIM900 IComSat 1.1 predkosc to 9600
  */

  // Restart trwa dosyc długo
  // Aby pominac, wywolaj init() zamiast restart()
  modem.init();

  String modemInfo = modem.getModemInfo();
  SerialMon.println(modemInfo);

  // Odblokuj karte SIM za pomoca kodu PIN jesli zablokowana
  //modem.simUnlock("1234");

  if (!modem.waitForNetwork()) {}
  SerialMon.println("Network Ok");

  if (!modem.gprsConnect(apn, user, pass)) {}
  SerialMon.println("GPRS OK");

  start_GPS();

  // MQTT Broker setup
  mqtt.setServer(broker, 1883);

  // ********************** CAN **********************
  CAN_InitStageOne();

  attachInterrupt(CAN_INTERRUPT_NUMBER, CAN_Interrupt, FALLING); // wlaczenie przrwan od modulu CAN

  uint8_t test[8];
  test[0] = 1;
  test[1] = 2;
  test[2] = 3;
  delay(100);

  //////////////////////////////////////////////////////////////////////////////
  //wysylanie po restarcie
  message.rtr = CAN_DATA;
  message.id = CAN_ID_START_MODULE_TELEMETRY;
  //CAN.LoadBuffer(buffer, &message);
  CAN.EnqueueTX(message);
  delay(100);
  /////////////////////////////////////////////////////////////////////////////
}

boolean mqttConnect()
{
  detachInterrupt(CAN_INTERRUPT_NUMBER); // wlaczenie przrwan od modulu CAN
  SerialMon.println("Detach");

  if (!mqtt.connect("SmartPower MQTT Mosquitto test"))
  {
    attachInterrupt(CAN_INTERRUPT_NUMBER, CAN_Interrupt, FALLING); // wlaczenie przrwan od modulu CAN
    return false;
  }
  mqtt.publish(topicDebug, "Modul telemetrii uruchomiony");
  SerialMon.println("Debug Sent");

  SP_mqttSending();

  attachInterrupt(CAN_INTERRUPT_NUMBER, CAN_Interrupt, FALLING); // wlaczenie przrwan od modulu CAN
  SerialMon.println("Attach");
  return mqtt.connected();
}
//--------------------------------------------------------------------------------------------------------------



//--------------------------------------------------------------------------------------------------------------
// *************************************************** LOOP ****************************************************
//--------------------------------------------------------------------------------------------------------------
void loop()
{

  if (mqtt.connected())
  {
    if (millis() - currentTime_Enabled >= ENABLE_TIME)
    {
      mqttConnect();
      for (int i = 0; i < 9 ; i++)
      {
        *dane[i] = 0;
      }
    }
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    /*	if(flagaPrad==DENY & millis() - currentTime_pradEnabled >= PRAD_TIME_ENABLE)
    	{
    		flagaPrad=ACCEPT;
    	}
    	/////////////////////////////////////////////////////
    	if(flagaNapiecie==DENY & millis() - currentTime_napiecieEnabled >= NAPIECIE_TIME_ENABLE)
    	{
    		flagaNapiecie=ACCEPT;
    	}
    	/////////////////////////////////////////////////////
    	if(flagaPredkosc==DENY & millis() - currentTime_predkoscEnabled >= PREDKOSC_TIME_ENABLE)
    	{
    		flagaPredkosc=ACCEPT;
    	}
    	/////////////////////////////////////////////////////
    	if(flagaTemp==DENY & millis() - currentTime_tempEnabled >= TEMP_TIME_ENABLE)
    	{
    		flagaTemp=ACCEPT;
    	}*/
    /////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //ZAPYTANIA
    /////////////////////////////////////////////////////
    //wyslanie zapytania o temperature co zadany czas
    /*	if (millis() - currentTime_temp >= TEMP_TIME)
    	{
    		currentTime_temp = millis();
    		message.rtr = CAN_RTR;
    		message.id = CAN_ID_SEND_CELL_TEMPERATURE;
    		//CAN.LoadBuffer(buffer, &message);
    		CAN.EnqueueTX(message);
    		delay(100);
    	}
    	/////////////////////////////////////////////////////
    	//wyslanie zapytania o czas co zadany czas
    	if (millis() - currentTime_time >= TEMP_TIME)
    	{
    		currentTime_time = millis();
    		message.rtr = CAN_RTR;
    		message.id = CAN_ID_TIME_SYNC_ASK;
    		//CAN.LoadBuffer(buffer, &message);
    		CAN.EnqueueTX(message);
    		delay(100);
    	}*/
    /////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  }
  else
  {
    // Reconnect every 10 seconds
    unsigned long t = millis();
    if (t - lastReconnectAttempt > 10000L)
    {
      lastReconnectAttempt = t;
      if (mqttConnect())
      {
        lastReconnectAttempt = 0;
      }
    }
  }
}

//--------------------------------------------------------------------------------------------------------------
// *************************************************** MQTT ****************************************************
//--------------------------------------------------------------------------------------------------------------
void publish_mqtt()
{
  for (int i = 0; i < 9; i++)
  {
    mqtt.publish(topicSmartPower, *dane[i]);
    SerialMon.println(*dane[i]);
    SerialMon.println("Published *dane MQTT");
  }
  
  /*mqtt.publish(topicPrad, c_prad);                  delay(100);
    mqtt.publish(topicNapiecie, c_napiecie);          delay(100);
    mqtt.publish(topicPredkosc, c_predkosc);          delay(100);
    mqtt.publish(topicTemperatura, c_temperatura);    delay(100);
    mqtt.publish(topicOkrazenie, c_okrazenie);        delay(100);*/
}
//--------------------------------------------------------------------------------------------------------------

void SP_mqttSending()
{
  /*
    dtostrf(temperatura, 2, 2, c_temperatura);
    dtostrf(prad, 2, 2, c_prad);
    dtostrf(napiecie, 2, 2, c_napiecie);
    dtostrf(predkosc, 2, 2, c_predkosc);
  */

  strcpy(dane[2], "2");

  publish_mqtt();
  get_GPS();
  
  itoa(modemGPS_licznik, c_licznik, 4);
  mqtt.publish(topicDebug, c_licznik);              delay(100);
  SerialMon.println("Licznik: ");
  SerialMon.println(modemGPS_licznik);
  modemGPS_licznik = modemGPS_licznik + 1;
}
//--------------------------------------------------------------------------------------------------------------
/*
  void power_on()
  {
    digitalWrite(onModulePin,HIGH);
    delay(3000);
    digitalWrite(onModulePin,LOW);
  }
*/
//--------------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------------
// *************************************************** CAN *****************************************************
//--------------------------------------------------------------------------------------------------------------
void CAN_Interrupt(void)
{
  CAN.intHandler();
  if (CAN.GetRXFrame(message))
  {
    /////////////////////////////////////////////////
    //dane o pradzie
    if (message.id == CAN_ID_CURRENT & *dane[2] == NULL & *dane[3] == NULL)
    {
      *dane[2] = message.data.byte[0];
      *dane[3] = message.data.byte[1];
    }
    /////////////////////////////////////////////////
    //dane o napieciu
    if (message.id == CAN_ID_VOLTAGE & *dane[2] == NULL & *dane[3] == NULL)
    {
      *dane[4] = message.data.byte[0];
      *dane[5] = message.data.byte[1];
    }
    /////////////////////////////////////////////////
    //dane o predkosci
    if (message.id == CAN_ID_CAR_VELOCITY & *dane[2] == NULL & *dane[3] == NULL)
    {
      *dane[0] = message.data.byte[0];
      *dane[1] = message.data.byte[1];
    }
    /////////////////////////////////////////////////
    //dane o temperaturze
    /*if (message.id == CAN_ID_CELL_TEMPERATURE)
      {
    	temp_dze = message.data.byte[0];
    	temp_calk = message.data.byte[1];
      }*/
    /////////////////////////////////////////////////
    //dane o czasie
    //dla okrazen nie trzeba flagi bo i tak sa rzadko!!!!!!!!!!!!!!!!!!!!
    /*if (message.id == CAN_ID_TIME_LAP)
      {
    	okrazenie = message.data.byte[0];
    	czas_start.sek = message.data.byte[1];
    	czas_start.mnt = message.data.byte[2];
    	czas_okrazenia.sek = message.data.byte[3];
    	czas_okrazenia.mnt = message.data.byte[4];
      }*/
    /*
      CAN.EnqueueTX(message);*/
  }
}
//--------------------------------------------------------------------------------------------------------------

void SPI_Init(void)
{
  // Set up SPI Communication
  // dataMode can be SPI_MODE0 or SPI_MODE3 only for MCP2515
  SPI.setClockDivider(SPI_CLOCK_DIV2);
  SPI.setDataMode(SPI_MODE0);
  SPI.setBitOrder(MSBFIRST);
  SPI.begin();
}
//--------------------------------------------------------------------------------------------------------------

void CAN_InitStageOne(void)
{
  /*int count = 50;                                     // the max numbers of initializint the CAN-BUS, if initialize failed first!.
    do
    {
    if (CAN_OK == CAN.begin(CAN_500KBPS, MCP_16MHz))             // init can bus : baudrate = 500k
    {
    break;
    }
    else
    {
    delay(10);
    if (count <= 1)
    {
    }
    }
    } while (count--); */

  SPI_Init();
  if (CAN.Init(CAN_BAUDRATE, CAN_CLK))
  {
  }
  else {
  }

  attachInterrupt(CAN_INTERRUPT_NUMBER, CAN_Interrupt, FALLING);
  CAN.InitFilters(false);
  CAN.SetRXMask(MASK0, 0x0, 0); //match all but bottom four bits
  CAN.SetRXFilter(FILTER0, 0x100, 0);
  /*
    allows 0x100 - 0x10F
    So, this code will only accept frames with ID of 0x100 - 0x10F. All other frames
    will be ignored.
    CAN.SetRXMask(MASK0, 0x0, 0); //match all but bottom four bits
    CAN.SetRXFilter(FILTER1, 0x514, 0);//od 1300
  */
}
//--------------------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------------------
// *************************************************** GPS *****************************************************
//--------------------------------------------------------------------------------------------------------------

int8_t sendATcommand(char* ATcommand, char* expected_answer, unsigned int timeout) {

  uint8_t x = 0,  answer = 0;
  char response[100];
  unsigned long previous;

  memset(response, '\0', 100);    // Initialice the string

  delay(100);

  while ( SerialAT.available() > 0) SerialAT.read();   // Clean the input buffer

  if (ATcommand[0] != '\0')
  {
    SerialAT.println(ATcommand);    // Send the AT command
    Serial.println(ATcommand);
  }


  x = 0;
  previous = millis();

  // this loop waits for the answer
  do {
    if (SerialAT.available() != 0) {  // if there are data in the UART input buffer, reads it and checks for the asnwer
      response[x] = SerialAT.read();
      SerialMon.print(response[x]);
      x++;
      if (strstr(response, expected_answer) != NULL)    // check if the desired answer (OK) is in the response of the module
      {
        answer = 1;
      }
    }
  } while ((answer == 0) && ((millis() - previous) < timeout));   // Waits for the asnwer with time out

  return answer;
}

int8_t start_GPS() {

  unsigned long previous;

  previous = millis();
  // starts the GPS
  sendATcommand("AT+CGPSPWR=1", "OK", 2000);
  sendATcommand("AT+CGPSRST=0", "OK", 2000);

  // waits for fix GPS
  while (( (sendATcommand("AT+CGPSSTATUS?", "2D Fix", 5000) ||
            sendATcommand("AT+CGPSSTATUS?", "3D Fix", 5000)) == 0 ) &&
         ((millis() - previous) < 90000));

  if ((millis() - previous) < 90000)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

int8_t get_GPS() {

  int8_t counter, answer;
  long previous;

  // First get the NMEA string
  // Clean the input buffer
  while ( SerialAT.available() > 0) SerialAT.read();
  // request Basic string
  sendATcommand("AT+CGPSINF=0", "AT+CGPSINF=0\r\n\r\n", 2000);

  counter = 0;
  answer = 0;
  memset(frame, '\0', 100);    // Initialize the string
  previous = millis();
  // this loop waits for the NMEA string
  do {

    if (SerialAT.available() != 0) {
      frame[counter] = SerialAT.read();
      counter++;
      // check if the desired answer is in the response of the module
      if (strstr(frame, "OK") != NULL)
      {
        answer = 1;
      }
    }
    // Waits for the asnwer with time out
  }
  while ((answer == 0) && ((millis() - previous) < 1000));

  frame[counter - 3] = '\0';

  // Parses the string
  strtok(frame, ",");
  strcpy(longitude, strtok(NULL, ",")); // Gets longitude
  strcpy(latitude, strtok(NULL, ",")); // Gets latitude
  strtok(NULL, ",");

  SerialMon.println("Long:");
  SerialMon.println(longitude);
  SerialMon.println("Lati:");
  SerialMon.println(latitude);

  convert2Degrees(longitude);
  convert2Degrees(latitude);

  dane[6] = longitude;
  dane[7] = latitude;

  return answer;
}

/* convert2Degrees ( input ) - performs the conversion from input
   parameters in  DD°MM.mmm’ notation to DD.dddddd° notation.
   Sign '+' is set for positive latitudes/longitudes (North, East)
   Sign '-' is set for negative latitudes/longitudes (South, West)
*/


int8_t convert2Degrees(char* input) {

  float deg;
  float minutes;
  boolean neg = false;

  //auxiliar variable
  char aux[10];

  if (input[0] == '-')
  {
    neg = true;
    strcpy(aux, strtok(input + 1, "."));

  }
  else
  {
    strcpy(aux, strtok(input, "."));
  }

  // convert string to integer and add it to final float variable
  deg = atof(aux);

  strcpy(aux, strtok(NULL, '\0'));
  minutes = atof(aux);
  minutes /= 1000000;
  if (deg < 100)
  {
    minutes += deg;
    deg = 0;
  }
  else
  {
    minutes += int(deg) % 100;
    deg = int(deg) / 100;
  }

  // add minutes to degrees
  deg = deg + minutes / 60;


  if (neg == true)
  {
    deg *= -1.0;
  }

  neg = false;

  if ( deg < 0 ) {
    neg = true;
    deg *= -1;
  }

  float numberFloat = deg;
  int intPart[10];
  int digit;
  long newNumber = (long)numberFloat;
  int size = 0;

  while (1) {
    size = size + 1;
    digit = newNumber % 10;
    newNumber = newNumber / 10;
    intPart[size - 1] = digit;
    if (newNumber == 0) {
      break;
    }
  }

  int index = 0;
  if ( neg ) {
    index++;
    input[0] = '-';
  }
  for (int i = size - 1; i >= 0; i--)
  {
    input[index] = intPart[i] + '0';
    index++;
  }

  input[index] = '.';
  index++;

  numberFloat = (numberFloat - (int)numberFloat);
  for (int i = 1; i <= 6 ; i++)
  {
    numberFloat = numberFloat * 10;
    digit = (long)numberFloat;
    numberFloat = numberFloat - digit;
    input[index] = char(digit) + 48;
    index++;
  }
  input[index] = '\0';


}