//////////////////////////////// UWAGI ///////////////////////////////////////
//Nalezy zastanowic sie nad przeniesiem scalania danych odebranych w dane wysylane gdzies indziej niz funkcja przerwania
//////////////////////////////////////////////////////////////////////////////



/*
* Subskrypcja wiadomosci w mosquitto-clients (sudo apt-get install mosquitto-clients:
*
* Debug:
*   mosquitto_sub -h test.mosquitto.org -t SmartPower/debug -q -l
*
* Dane telemetryczne:
*
mosquitto_sub -h test.mosquitto.org -t SmartPower/prad -t SmartPower/temperatura
-t SmartPower/okrazenie -t SmartPower/czasOkrazenia -t SmartPower/GPS -q -l
*
*/

#define SerialMon Serial
/*
* Ustawienie serialu dla debugu w konsoli (dla Serial Monitora, bazowa predkosc to 115200)
*
*/

#define SerialAT Serial1
/*
*  Hardware Serial uzywany na Mega, Leonardo, Micro
*  Gdy modul ma zworki (SIM900 IComSat v1.1 bez GPS)
*  TEORETYCZNIE na WaveShare GSM/GPRS/GPS Shield (B) SIM808 powinno dzialac,
*  ALE modul zaslania Serial1 :v
*
*/

//#include <SoftwareSerial.h>
//SoftwareSerial SerialAT(2, 3); // RX, TX
/*
*  Softwarowy Serial
*  Dla modulu WaveShare GSM/GPRS/GPS Shield (B) SIM808
*  W sensie, ze nie dziala...
*
*/

#define TINY_GSM_MODEM_SIM900
//#define TINY_GSM_MODEM_SIM808
/*
* Definicja modulu ktory jest aktualnie uzywany
* Przy zmianie konieczna jest zmiana w bibliotece TinyGsmClientSIM800.h
* Dopisanie lub usuniecie int w linijce 761
* Poprawna dla SIM808: rsp = waitResponse(75000L,
* Poprawna dla SIM900: int rsp = waitResponse(75000L,
*
*/

#include <stdlib.h>

#include <TinyGsmClient.h>
/*
* Biblioteka zawierajaca obsluge MQTT
*
*/
#include <PubSubClient.h>
/*
* Publikacja i Subskrypcja
*
*/

//#include "GetGPS.h"
/*
* Wywala bledy, do poprawki
* Definicje funkcji zwiazanych z GPSem
*
*/

//#include <DFRobot_sim808.h>
/*
* Biblioteka DFRobot dla SIM808
* Zawiera obsluge GPS
*
*/
//DFRobot_SIM808 sim808(&Serial1);

//int onModulePin = 1;

//do ustalenia czasy co jakie ma byc wysylane zadanie podania aktualnych danych
//chyba ze w jakis inny sposob bedzie to zrobione (np.reakcja na akcje z aplikacji z panelu)
#define TEMP_TIME 10000
#define TIME_TIME 10000
#include <SPI.h>
#include <MCP2515.h>
#include "Can_ID.h"
#define CAN_CLK 8 //kwarc
//#define CAN_BAUD 250 //predkosc CAN
#define CAN_SPI_MCP_PIN  22 //pin podlaczenia interfejsu spi
#define CAN_INTERRUPT_PIN 21 //podlaczenie interupta 
#define CAN_INTERRUPT_NUMBER 2 //numer interupta
#define CAN_RTR 1 //czy ramka z danymi czy remote
#define CAN_DATA 0
MCP2515 CAN(CAN_SPI_MCP_PIN, CAN_INTERRUPT_PIN);
Frame message;
uint8_t buffer;

//****************************prototypy funckji*************************//
void CAN_Interrupt(void); //przerwanie wywo³ywane przy przyjœciu wiadomoœci
void CAN_InitStageOne(void); //pierwszy etap inicjalizacji CAN
void SPI_Init(void); //inicjalizacja SPI na potrzeby CAN
					 /////////////////////////////////////////////////////////////

//--------------------------------------------------------------------------------------------------------------
// *************************************************** MQTT ****************************************************
//--------------------------------------------------------------------------------------------------------------

const char apn[] = "internet"; //T-Mobile
const char user[] = "";
const char pass[] = "";
/*
* Dane APN karty SIM
* Pozostawić puste, jesli user albo password sa nieuzywane
*
*/

// ******* Detale MQTT *******
const char* broker = "test.mosquitto.org";
/*
* Adres brokera MQTT
* test.mosquitto.org jest publiczny
*
*/
const char* topicDebug = "SmartPower/debug";

// ******* Definicje topiców z poszczególnymi parametrami *******
const char* topicPrad = "SmartPower/prad";
const char* topicTemperatura = "SmartPower/temperatura";
const char* topicOkrazenie = "SmartPower/okrazenie";
const char* topicCzasOkrazenia = "SmartPower/czasOkrazenia";
const char* topicSzerokosc = "SmartPower/Szerokosc";
const char* topicDlugosc = "SmartPower/Dlugosc";
const char* topicPredkosc = "SmartPower/Predkosc";

// ******* Definicje zmiennych poszczególnych parametrów *******
double prad = 1.0;
char c_prad[8];

double temperatura = 2.0;
char c_temperatura[8];

char c_predkosc[8];

int okrazenie = 3;
char c_okrazenie[8];


// ******* GPS *******
struct s_szerokosc
{
  char stopnie[4];
  char minuty[4];
  char sekundy[8];
}szerokosc;

struct s_dlugosc
{
  char stopnie[4];
  char minuty[4];
  char sekundy[8];
}dlugosc;

struct s_data
{
  char* rok;
  char* miesiac;
  char* dzien;
  char* godzina;
  char* minuta;
  char* sekunda;
  char* milisek;
}data;

/*
* Wartosci przypisane do testow
*
*/

int modemGPS_licznik = 1;

TinyGsm modem(SerialAT);
TinyGsmClient client(modem);
PubSubClient mqtt(client);

long lastReconnectAttempt = 0;
//--------------------------------------------------------------------------------------------------------------

// ******* Definicje zmiennych poszczególnych parametrów *******
//////////////////////////////////////
//PRAD
//zmienne do wysylania
//float prad = 0.0;
//zmienne do odebrania
int prad_calk = 0;
int prad_dze = 0;
//////////////////////////////////////
//NAPIECIE
//zmienne do wysylania
float napiecie = 0.0;
//zmienne do odebrania
int nap_calk = 0;
int nap_dze = 0;
//////////////////////////////////////
//TEMPERATURA
unsigned long currentTime_temp = millis();//pobranie czasu poczatkowego do wysylania zadania aktualizacji temperatury
//zmienne do wysylania
//float temperatura = 0.0;
//zmienne do odebrania
int temp_calk = 0;
int temp_dze = 0;
//////////////////////////////////////
//PREDKOSC
//zmienne do wysylania
float predkosc = 0.0;
//zmienne do odebrania
int pred_calk = 0;
int pred_dze = 0;
//////////////////////////////////////
//CZASY
unsigned long currentTime_time = millis(); //pobranie czasu poczatkowego do wysylania zadania aktualizacji czasu 
//zmienne do wysylania
//int okrazenie = 0;
//czasy okrazenia
//zmienne do wysylania
//struct TIME czas_okrazenia;
//zmienne do odebrania
int sec_okr = 0;
int min_okr = 0;
//czasy od startu
//zmienne do wysylania
//struct TIME czas_start;
//zmienne do odebrania
int sec_start = 0;
int min_start = 0;
//////////////////////////////////////
//GPS
//do zmiany po ogarnieciu GPS
//int dlugosc = 1;
//int szerokosc = 1;
//////////////////////////////////////
int modemCounter = 0;

//TinyGsm modem(SerialAT);
//TinyGsmClient client(modem);
//PubSubClient mqtt(client);

//long lastReconnectAttempt = 0;

//funckja do skladania zmiennych odebranych w calosc do wyslania
float assemble(int cz_calk, int cz_dze)
{
	float liczba = (float)(cz_dze / 100);
	liczba = liczba + cz_calk;
	return liczba;
}

//--------------------------------------------------------------------------------------------------------------
// ************************************************** SETUP ****************************************************
//--------------------------------------------------------------------------------------------------------------
void setup()
{
	// Ustawienie predkosci konsoli
	SerialMon.begin(115200);
	delay(100);

  

	// ********************** CAN **********************
	SerialMon.println("Inicjalizuje CAN");
	CAN_InitStageOne();

	SerialMon.println("Inicjalizuje Przerwania");
	attachInterrupt(CAN_INTERRUPT_NUMBER, CAN_Interrupt, FALLING); // wlaczenie przrwan od modulu CAN

	SerialMon.println("Zakonczenie inicjalizacji");
	SerialMon.println("Wysylanie ramek kontrolnych");
	uint8_t test[8];
	test[0] = 1;
	test[1] = 2;
	test[2] = 3;
	delay(100);

	SerialMon.println("===================================================");
	//////////////////////////////////////////////////////////////////////////////
	//wysylanie po restarcie
	message.rtr = CAN_DATA;
	message.id = CAN_ID_START_MODULE_TELEMETRY;
	//CAN.LoadBuffer(buffer, &message);
	SerialMon.println(message.id);
	CAN.EnqueueTX(message);
	delay(100);
	/////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////
  delay(1000);
  
  // ********************** MODEM **********************
  SerialAT.begin(9600);
  delay(100);
  /*
  * Ustawienie predkosci modulu GSM
  * Sprawdzanie domyslnej za pomoca sketchu AT_Debug
  * Dla modułu SIM900 IComSat 1.1 predkosc to 9600
  */

  // Restart trwa dosyc długo
  // Aby pominac, wywolaj init() zamiast restart()
  SerialMon.println("Inicjalizuje Modem...");
  modem.init();

  String modemInfo = modem.getModemInfo();
  SerialMon.print("Modem: ");
  SerialMon.println(modemInfo);

  // Odblokuj karte SIM za pomoca kodu PIN jesli zablokowana
  //modem.simUnlock("1234");

  SerialMon.print("Oczekiwanie na siec...");
  if (!modem.waitForNetwork())
  {
    SerialMon.println(" blad");
    while (true);
  }
  SerialMon.println(" OK");

  SerialMon.print("Laczenie do ");
  SerialMon.print(apn);

  if (!modem.gprsConnect(apn, user, pass))
  {
    SerialMon.println(" blad");
    while (true);
  }
  SerialMon.println(" OK");

  // MQTT Broker setup
  mqtt.setServer(broker, 1883);
  mqtt.setCallback(mqttCallback);
}

boolean mqttConnect()
{
  SerialMon.print("Laczenie z ");
  SerialMon.print(broker);
  if (!mqtt.connect("SmartPower MQTT Mosquitto test"))
  {
    SerialMon.println(" blad");
    return false;
  }
  SerialMon.println(" OK");
  mqtt.publish(topicDebug, "modul telemetri SmartPower uruchomiony");
  return mqtt.connected();
   
}
//--------------------------------------------------------------------------------------------------------------



//--------------------------------------------------------------------------------------------------------------
// *************************************************** LOOP ****************************************************
//--------------------------------------------------------------------------------------------------------------
void loop()
{
	/////////////////////////////////////////////////////
	//wyslanie zapytania o temperature co zadany czas
	if (millis() - currentTime_temp >= TEMP_TIME)
	{
		currentTime_temp = millis();

		message.rtr = CAN_RTR;
		message.id = CAN_ID_SEND_CELL_TEMPERATURE;
		//CAN.LoadBuffer(buffer, &message);
		SerialMon.println(message.id);
		CAN.EnqueueTX(message);
		delay(100);
	}
	/////////////////////////////////////////////////////
	//wyslanie zapytania o czas co zadany czas
	if (millis() - currentTime_time >= TEMP_TIME)
	{
		currentTime_time = millis();

		message.rtr = CAN_RTR;
		message.id = CAN_ID_TIME_SYNC_ASK;
		//CAN.LoadBuffer(buffer, &message);
		SerialMon.println(message.id);
		CAN.EnqueueTX(message);
		delay(100);
	}
	/////////////////////////////////////////////////////
	if (mqtt.connected())
	{
		mqtt.loop();
	}
	else
	{
		// Reconnect every 10 seconds
		unsigned long t = millis();
		if (t - lastReconnectAttempt > 10000L)
		{
			lastReconnectAttempt = t;
			if (mqttConnect())
			{
				lastReconnectAttempt = 0;
			}
		}
	}
}

//--------------------------------------------------------------------------------------------------------------
// *************************************************** MQTT ****************************************************
//--------------------------------------------------------------------------------------------------------------
void publish_mqtt()
{
  mqtt.publish(topicPrad, c_prad);  delay(100);

  mqtt.publish(topicPredkosc, c_predkosc);  delay(100);

  mqtt.publish(topicTemperatura, c_temperatura);  delay(100);

  mqtt.publish(topicOkrazenie, c_okrazenie);  delay(100);
/*
  mqtt.publish(topicCzasOkrazenia, data.rok);     delay(100);
  mqtt.publish(topicCzasOkrazenia, data.miesiac); delay(100);
  mqtt.publish(topicCzasOkrazenia, data.dzien);   delay(100);
  mqtt.publish(topicCzasOkrazenia, data.godzina); delay(100);
  mqtt.publish(topicCzasOkrazenia, data.minuta);  delay(100);
  mqtt.publish(topicCzasOkrazenia, data.sekunda); delay(100);
  mqtt.publish(topicCzasOkrazenia, data.milisek); delay(100);

  mqtt.publish(topicSzerokosc, szerokosc.stopnie);  delay(100);
  mqtt.publish(topicSzerokosc, szerokosc.minuty);   delay(100);
  mqtt.publish(topicSzerokosc, szerokosc.sekundy);  delay(100);

  mqtt.publish(topicDlugosc, dlugosc.stopnie);  delay(100);
  mqtt.publish(topicDlugosc, dlugosc.minuty);   delay(100);
  mqtt.publish(topicDlugosc, dlugosc.sekundy);  delay(100);*/
}
//--------------------------------------------------------------------------------------------------------------

void mqttCallback()
{
  dtostrf(temperatura, 2, 2, c_temperatura);
  dtostrf(prad, 2, 2, c_prad);

  //GPS_meth();

  publish_mqtt();

  SerialMon.print(" Wyslano zestaw danych:");
  SerialMon.print(modemGPS_licznik);        delay(100);
  modemGPS_licznik = modemGPS_licznik + 1;
}
//--------------------------------------------------------------------------------------------------------------
/*
void GPS_meth()
{
  if (sim808.getGPS()) 
    {
      data.rok = (char*)sim808.GPSdata.year;             //uint16_t
      data.miesiac = (char*)sim808.GPSdata.month;        //uint8_t
      data.dzien = (char*)sim808.GPSdata.day;            //uint8_t
      data.godzina = (char*)sim808.GPSdata.hour;         //uint8_t
      data.minuta = (char*)sim808.GPSdata.minute;        //uint8_t
      data.sekunda = (char*)sim808.GPSdata.second;     	 //uint8_t
      data.milisek = (char*)sim808.GPSdata.centisecond;  //uint8_t
    
      sim808.latitudeConverToDMS();
      //"latitude :"
      itoa(sim808.latDMS.degrees,szerokosc.stopnie,2);      //int
      itoa(sim808.latDMS.minutes,szerokosc.minuty,2);       //int
      sprintf(szerokosc.sekundy,"%1.6f",sim808.latDMS.seconeds);  //float

      sim808.LongitudeConverToDMS();
      //"longitude :"
      itoa(sim808.longDMS.degrees,dlugosc.sekundy,2);       //int
      itoa(sim808.longDMS.minutes,dlugosc.sekundy,2);       //int
      sprintf(dlugosc.sekundy,"%6.6f",sim808.longDMS.seconeds);   //float
    
      //"speed_kph :"
      sprintf(c_predkosc,"%3.6f",sim808.GPSdata.speed_kph); //float

      //************* wylacz modul GPS ************
      sim808.detachGPS();
    }
}
//--------------------------------------------------------------------------------------------------------------

void power_on()
{

    uint8_t answer=0;

    digitalWrite(onModulePin,HIGH);
    delay(3000);
    digitalWrite(onModulePin,LOW);
}
//--------------------------------------------------------------------------------------------------------------
*/
//--------------------------------------------------------------------------------------------------------------
// *************************************************** CAN *****************************************************
//--------------------------------------------------------------------------------------------------------------
void CAN_Interrupt(void)
{
	CAN.intHandler();
	if (CAN.GetRXFrame(message))
	{
		/////////////////////////////////////////////////
		//dane o pradzie
		if (message.id == CAN_ID_CELL_CURRENT)
		{
			SerialMon.println("Prad ID:");
			SerialMon.println(message.id);

			prad_dze = message.data.byte[0];
			prad_calk = message.data.byte[1];
			prad=assemble(prad_calk, prad_dze);

		}
		/////////////////////////////////////////////////
		//dane o napieciu
		if (message.id == CAN_ID_CELL_VOLTAGE)
		{
			SerialMon.println("Napiecie ID:");
			SerialMon.println(message.id);

			nap_dze = message.data.byte[0];
			nap_calk = message.data.byte[1];
			napiecie = assemble(nap_calk, nap_dze);
		}
		/////////////////////////////////////////////////
		//dane o predkosci
		if (message.id == CAN_ID_CAR_VELOCITY)
		{
			SerialMon.println("Predkosc ID:");
			SerialMon.println(message.id);

			pred_dze = message.data.byte[0];
			pred_calk = message.data.byte[1];
			predkosc = assemble(pred_calk, pred_dze);
		}
		/////////////////////////////////////////////////
		//dane o temperaturze
		if (message.id == CAN_ID_CELL_TEMPERATURE)
		{
			SerialMon.println("Temperatura ID:");
			SerialMon.println(message.id);

			temp_dze = message.data.byte[0];
			temp_calk = message.data.byte[1];
			temperatura = assemble(temp_calk, temp_dze);
		}
		/////////////////////////////////////////////////
		//dane o czasie
		if (message.id == CAN_ID_TIME_LAP)
		{
			SerialMon.println("Czas ID:");
			SerialMon.println(message.id);

			okrazenie = message.data.byte[0];
			//czas_start.sec = message.data.byte[1];
			//czas_start.min = message.data.byte[2];
			//czas_okrazenia.sec = message.data.byte[3];
			//czas_okrazenia.min = message.data.byte[4];
			
		}
		/////////////////////////////////////////////////
		SerialMon.print("ID = ");
		SerialMon.print(message.id);

		SerialMon.print("\t");
		if (message.extended)
		{
		SerialMon.print("EXTENDED");
		}
		else
		{
		SerialMon.print("STANDARD");
		}

		SerialMon.print("\t");
		if (message.rtr)
		{
		SerialMon.println("REMOTE");
		}
		else
		{
		SerialMon.print("DATA");
		SerialMon.print("\t len:");
		SerialMon.println(message.length);
		for (int i = 0; i < message.length; i++)
		{
		SerialMon.print(message.data.byte[i], DEC);
		SerialMon.print("\t");
		}
		SerialMon.println();
		}
		SerialMon.println("===================================================");
		CAN.EnqueueTX(message);
	}
}
//--------------------------------------------------------------------------------------------------------------

void SPI_Init(void)
{
	// Set up SPI Communication
	// dataMode can be SPI_MODE0 or SPI_MODE3 only for MCP2515
	SPI.setClockDivider(SPI_CLOCK_DIV2);
	SPI.setDataMode(SPI_MODE0);
	SPI.setBitOrder(MSBFIRST);
	SPI.begin();
}
//--------------------------------------------------------------------------------------------------------------

void CAN_InitStageOne(void)
{
	/*int count = 50;                                     // the max numbers of initializint the CAN-BUS, if initialize failed first!.
	do
	{
	if (CAN_OK == CAN.begin(CAN_500KBPS, MCP_16MHz))             // init can bus : baudrate = 500k
	{
	SerialMon.println("CAN_InitStageOne - inicjalziacja powiodla sie");
	break;
	}
	else
	{
	SerialMon.println("CAN_InitStageOne - inicjalizacja modulu CAN nie powiodla sie");
	SerialMon.println("CAN_InitStageOne - ponowna proba inicjalizacji");
	delay(10);
	if (count <= 1)
	{
	SerialMon.println("CAN_InitStageOne - inicjalizacja modulu nie powiodla sie");
	}

	}

	} while (count--); */

	SPI_Init();
	if (CAN.Init(CAN_BAUDRATE, CAN_CLK))
	{
		SerialMon.println("MCP2515 Init OK ...");
	}
	else {
		SerialMon.println("MCP2515 Init Failed ...");
	}

	attachInterrupt(CAN_INTERRUPT_NUMBER, CAN_Interrupt, FALLING);
	CAN.InitFilters(false);
	CAN.SetRXMask(MASK0, 0x0, 0); //match all but bottom four bits
	CAN.SetRXFilter(FILTER0, 0x100, 0);
	/*
	* allows 0x100 - 0x10F
	* So, this code will only accept frames with ID of 0x100 - 0x10F. All other frames
	* will be ignored.
	* CAN.SetRXMask(MASK0, 0x0, 0); //match all but bottom four bits
	* CAN.SetRXFilter(FILTER1, 0x514, 0);//od 1300
	*/
}
//--------------------------------------------------------------------------------------------------------------
