
#ifndef _CAN_ID_h
#define _CAN_ID_h

//rozkazy
#define CAN_TURN_ON 2
#define CAN_TURN_OFF 0
#define CAN_TOGGLE 1

//statusy
#define CAN_STATUS_ON 3
#define CAN_STATUS_OFF 4
#define CAN_STATUS_UNKNOWN 5

#define CAN_BAUDRATE 250



//CAN IDs

//*************************************oswietlenie*************************************************************//
#define CAN_ID_LIGHTS 1300	//bazowe id dla oswietlenia zewnetrznego
#define CAN_ID_LIGHTS_BLIKERS CAN_ID_LIGHTS+20


//********swiatla pozycyjne*******//
#define CAN_ID_LIGHTS_COMMAND_PARKING CAN_ID_LIGHTS+1	//1301 //ID rozkazu wl/wyl/przel oswietlenia zewnetrznego
/*
 Sklada sie z jednego bajtu
 CAN_TURN_ON - swiatla powinny zosta� wlaczone
 CAN_TURN_OFF - swiatla powinny zosta� wylaczone
 CAN_TOGGLE - przelaczenie na stan przeciwny

 Wyslana jako ramka z danymi - rozkaz wl/wyl/przel swiatel pozycyjnych
 Odebranie jej przerz modul sterujacy oswietleniem musi powodowac wl/wyl/przel swiatel pozycyjnych.

 Wysyla dane - modul kierowcy
 Obiera dane - modul sterujacy oswietleniem
*/

#define CAN_ID_LIGHTS_ASK_COMMAND_PARKING CAN_ID_LIGHTS+2	//1302	//prosba o ponowne przeslanie rozkazu wl/wyl/przel
/*
 Wyslana jako ramka Remote Request - zadanie odeslania ramki CAN_ID_LIGHTS_COMMAND_PARKING
 	 z aktualnym statusem swiatel pozycyjnych.
 	 Odebranie jej przez modul kt�ry ZNA aktualny stan swiatel zewnetrznych musi powodowac
 	 odeslanie ramki CAN_ID_LIGHTS_COMMAND_LIGHTS.

 Wysyla pytanie - modul sterujacy oswietleniem np. po restarcie
 Odbiera pytanie - modul ZNAJACY stan swiatel pozycyjnych - modul kierowcy
 */


#define CAN_ID_LIGHTS_STATUS_PARKING CAN_ID_LIGHTS+3 //1303	//Prosba o stan swiatel pozycyjnych
/*
 Ramka z danymi
 Sklada sie z jednego bajtu
 CAN_STATUS_ON	- swiatla wlaczone
 CAN_STATUS_OFF	- swiatla wylaczone

 Ramka z danymi - powiadamia o tym, czy swiatla sa wlaczone/wylaczone.
 	 Odebranie tej ramki jest potwierdzniem wlaczenia lub nie swiatel pozycyjnych.

 Zawsze zawiera informacje o obecnym statusie swiatel pozycyjnych.
 Jest to potwierdzeie dzialania swiatel pozycyjnych.

 Wysyla dane - modul ZNAJACY stan swiatel pozycyjnych - modul oswietlenia, modul oswietlenia przy kazdej zmianie statusu
 Obiera dane - kazdy zainteresowany modul - np. modul kierowcy
*/


#define CAN_ID_LIGHTS_ASK_STATUS_PARKING  CAN_ID_LIGHTS+4 //1304
/*
 Ramka typu Remote Request. Po odebraniu nalezy odeslac ramke CAN_ID_LIGHTS_STATUS_PARKING
 (tylko  przez modul kt�ry ZNA obecny status).


 Wysyla pytanie - kazdy zainteresowany modul - np modul kierowcy
 Odbiera pytanie - modul ZNAJACY stan swiatel pozycyjnych -
*/




//********kierunkowskazy*******//
#define CAN_ID_LIGHTS_BLIKERS CAN_ID_LIGHTS+20//1320
//prawy kierunkowskaz
#define CAN_ID_LIGHTS_COMMAND_R_BLINKER CAN_ID_LIGHTS_BLIKERS+1 //1321
/*
 * Rozkaz wlaczenia prawego kierunkowskazu
 Sklada sie z jednego bajtu
 CAN_TURN_ON - prawy kierunkowskaz powinien zostac wlaczony na 3 migniecia

 Wysyla dane - modul kierowcy
 Obiera dane - modul sterujacy oswietleniem,

*/
#define CAN_ID_LIGHTS_COMMAND_L_BLINKER CAN_ID_LIGHTS_BLIKERS+2 //1322
/*
 * Rozka wlaczenia lewego kierunkowskazu
Sklada sie z jednego bajtu
CAN_TURN_ON - lewy kierunkowskaz powinien zostac wlaczony na 3 migniecia

Wysyla dane - modul kierowcy
Obiera dane - modul sterujacy oswietleniem,

*/
#define CAN_ID_LIGHTS_COMMAND_HAZARD_BLINKER CAN_ID_LIGHTS_BLIKERS+3 //1323
/*
 * Rozkaz wlaczenia kierunkowskazow awaryjnych
 * Ramka typu dane
 * Sklada sie z jednego bajtu
 * CAN_TURN_ON - wlaczenie kierunkowskazow awaryjnych
 * CAN_TURN_OFF - wylaczenie kierunkowskazow awaryjnych
 * CAN_TURN_TOGGLE - przelaczenie kierunkowskazow awaryjnych
 *
 * Wyslana jako ramka z danymi - rozkaz wl/wyl/przel migania kierunkowskazow
 * Odebranie jej przez modul sterujacy oswietleniem musi powodowac wl/wyl/przel migania kierunkowskaz�w.
 * Wysyla dane - modul kierowcy
 * Obiera dane - modul sterujacy oswietleniem,
*/

#define CAN_ID_LIGHTS_ASK_COMMAND_HAZARD_BLINKER CAN_ID_LIGHTS_BLIKERS+4 //1324
/*
 * Wyslana jako ramka Remote Request - zadanie odeslania ramki CAN_ID_LIGHTS_COMMAND_HAZARD_BLINKER
 * z aktualnym poleceniem swiatel awaryjnych
 * Odebranie jej przez modul kt�ry ZNA aktualny stan kierunkowskazu musi powodowac
 * odeslanie ramki CAN_ID_LIGHTS_COMMAND_R_BLINKER.
 *
 * Wysyla pytanie - modul sterujacy oswietleniem
 * Odbiera pytanie - modul ZNAJACY stan kierunkowskaz�w	- modul kierowcy
*/

#define CAN_ID_LIGHTS_ASK_STATUS_HAZARD_BLINKER CAN_ID_LIGHTS_BLIKERS+5 //1325
/*
 * Pytanie o status swiatel awaryjnych
 * Przesylane jako ramka Remote Request
 * Odebranie jej przez modul oswietlenia musi powodowac odeslanie aktualnego statusu
 * swiatel awaryjnych CAN_ID_LIGHTS_STATUS_HAZARD_BLINKER
 *
 * Wysyla pytanie - modul kierowcy/inny zainteresowany
 * Odbiera pytanie - modul oswietlenia
 */

#define CAN_ID_LIGHTS_STATUS_HAZARD_BLINKER CAN_ID_LIGHTS_BLIKERS+6 //1326
/*
 * Ramka z danymi o obecnym statusie swiatel awaryjnych
 * Przesylane jako ramka z dannymi
 * Wysylana kazdorazowo przy zmanie statusu swiatel awaryjnych
 *
 *
 * Wysyla dane - modul oswietlenia
 * Odbiera dane - modul kierowcy
 */



//*********inne wejscia***********//
#define CAN_ID_BRAKE_STATUS 1000
/*
 * Ramka z danymi wysylana przy kazdej zmianie stanu czujnika hamulca lub jako odpowiedz na CAN_ID_BRAKE_ASK_STATUS
 * Zawiera jeden bajt
 * CAN_TURN_ON - hamulec nozny wcisniety
 * CAN_TURN_OFF - hamulec nozny NIE wcisniety
 * Wysyla dane - modul kierowcy
 */

#define CAN_ID_BRAKE_ASK_STATUS CAN_ID_BRAKE_STATUS+1 //1001
/*
 * Ramka wysylana jako RTR, jej odebranie przez modul kierowcy musi powodowac odeslanie CAN_ID_BRAKE_STATUS
 * Wysyla pytanie - kazdy zaintereesowany - modul oswietlenia
 * Odbiera pytanie - modul kierowcy
 */

#define CAN_ID_EMERGENCY_SWITCH_STATUS 1002  //1002
/*
 * Ramka z danymi wysylana przy kazdej zmianie stanu przycisku bezpieczenstwa lub jako odpowiedz na CAN_ID_EMERGENCY_SWITCH_ASK_STATUS
 * Zawiera jeden bajt
 * CAN_TURN_ON - przycisk wcisniety
 * CAN_TURN_OFF - przycisk NIE wcisniety
 * Wysyla dane - modul kierowcy
 */

#define CAN_ID_EMERGENCY_SWITCH_ASK_STATUS CAN_ID_EMERGENCY_SWITCH_STATUS+1 //1003
/*
 * Ramka wysylana jako RTR, jej odebranie przez modul kierowcy musi powodowac odeslanie CAN_ID_EMERGENCY_SWITCH_STATUS
 * Wysyla pytanie - kazdy zaintereesowany
 * Odbiera pytanie - modul kierowcy
 */

#define CAN_ID_WIPER_SWITCH_STATUS 1100
/*
 * Ramka z danymi wysylana przy kazdej zmianie stanu przelacznika wycieraczek lub jako odpowiedz na CAN_ID_WIPER_SWITCH_ASK_STATUS
 * Zawiera jeden bajt
 * CAN_TURN_ON - wycieraczka ma byc wlaczona
 * CAN_TURN_OFF - wycieraczka ma byc wylaczona
 * Wysyla dane - modul kierowcy
 */

#define CAN_ID_WIPER_SWITCH_ASK_STATUS CAN_ID_WIPER_SWITCH_STATUS+1 //1101
/*
 * Ramka wysylana jako RTR, jej odebranie przez modul kierowcy musi powodowac odeslanie CAN_ID_WIPER_SWITCH_STATUS
 * Wysyla pytanie - kazdy zaintereesowany - modul oswietlenia
 * Odbiera pytanie - modul kierowcy
 */


/*
 * **************************************DANE****************************************
 */


//*************************************prad ogniwa*************************************************************//
#define CAN_ID_CELL_CURRENT 1400    //bazowe id dla temperatury ogniwa

/************************DO UZUPELNIENIA
* Zawiera informacje o pradzie ogniwa.
* bajt 1 -> czesc po przecinku
* bajt 2 -> czesc calkowita
* Wysyla:
* Modul w C
* Odbierany przez:
* telemetria
Nie wymaga potwierdzenia odebrania.
*/

//*************************************temperatura ogniwa*************************************************************//
#define CAN_ID_CELL_TEMPERATURE 1410    //bazowe id dla temperatury ogniwa
/************************DO UZUPELNIENIA
* Zawiera informacje o predkosci.
* bajt 1 -> czesc po przecinku
* bajt 2 -> czesc calkowita
* Wysyla:
* Modul metrii
* Odbierany przez:
* telemetria
Nie wymaga potwierdzenia odebrania.
*/
#define CAN_ID_SEND_CELL_TEMPERATURE    CAN_ID_CELL_TEMPERATURE+1	//2011
/*
Wysylana jako ramka Remote Request - zadanie odeslania ramki CAN_ID_SEND_CELL_TEMPERATURE
z aktualna wartoscia temperatury ogniwa z dwoch czujnikow.
Odebranie jej przez modul kt�ry ZNA aktualna wartosc temperatury musi powodowac
odeslanie ramki CAN_ID_SEND_CELL_TEMPERATURE.
* Wysyla:
* Modul 
* Odbierany przez:
* telemetria
Nie wymaga potwierdzenia odebrania.
*/

//*************************************napiecie ogniwa*************************************************************//
#define CAN_ID_CELL_VOLTAGE 1450    //bazowe id dla napiecia ogniwa
/************************DO UZUPELNIENIA
* Zawiera informacje o napieciu ogniwa.
* bajt 1 -> czesc po przecinku
* bajt 2 -> czesc calkowita
* Wysyla:
* Modul w C
* Odbierany przez:
* telemetria
Nie wymaga potwierdzenia odebrania.
*/


//*************************************predkosc bolidu*************************************************************//
#define CAN_ID_CAR_VELOCITY 1420    //bazowe id dla predkosci
/************************DO UZUPELNIENIA
* Zawiera informacje o predkosci.
* bajt 1 -> czesc po przecinku
* bajt 2 -> czesc calkowita
* Wysyla:
* Modul metrii
* Odbierany przez:
* telemetria
Nie wymaga potwierdzenia odebrania.
*/

//*************************************czas i numer okrazenia*************************************************************//
#define CAN_ID_TIME 1430
/*
 * Zawiera informacje o obecnej godzinie i dacie .
 * Wysylana przez modul kierowcy tylko jako odpowiedz na prosbe
 * Tzn. Po odebraniu zapytania CAN_ID_TIME_ASK
 * Ramka typu DATA, dlugosc 6 bajt�w
 * bajt 1 -> sekunda
 * bajt 2 -> minuta
 * bajt 3 -> godzina
 * bajt 4 -> dzie�
 * bajt 5 -> miesiac
 * bajt 6 -> rok
 * Wysyla:
 * 	Modul kierowcy
 * Odbierany przez:
 */

#define CAN_ID_TIME_ASK CAN_ID_TIME+1	//1431
/*
 * Pytanie o CAN_ID_TIME
 * Ramka typu RTR, wysylana przez modul zaninteresowany aktualna data
 * Modul kierowcy odpowiada ramka CAN_ID_TIME
 * Kto wysyla ramke RTR/pyta:
 *
 * Odpowiada/Pytanie odbierane przez:
 * 	Modul kierowcy
 */


#define CAN_ID_TIME_SYNC CAN_ID_TIME_ASK+1	//1432
/*
 * Zawiera informacje o obecnej godzinie i dacie .
 * Wysylana przez modul kierowcy tylko na prosbe i tylko w momecie dodania kolejnej sekundy
 * Tzn. Po odebraniu zapytania CAN_ID_TIME_SYNC_ASK modul kierowcy czeka na moment pelnej sekundy i odsyla ta ramke
 * Ramka typu DATA, dlugosc 6 bajt�w
 * bajt 1 -> sekunda
 * bajt 2 -> minuta
 * bajt 3 -> godzina
 * bajt 4 -> dzie�
 * bajt 5 -> miesiac
 * bajt 6 -> rok
 * Wysyla:
 * 	Modul kierowcy
 * Odbieramy przez:
 *
 */

#define CAN_ID_TIME_SYNC_ASK CAN_ID_TIME_SYNC +1 //1433
/*
 * Pytanie o CAN_ID_TIME_SYNC
 * Ramka typu RTR, wysylana przez modul zaninteresowany aktualna data oraz synchronizacja czasu
 * Modul kierowcy odpowiada ramka CAN_ID_TIME_SYNC
 * Kto wysyla ramke RTR/pyta:
 *
 * Odpowiada/Pytanie odbierane przez:
 * 	Modul kierowcy
 */


#define CAN_ID_TIME_LAP CAN_ID_TIME_SYNC_ASK +1 //1434
/*
 * Zawiera informacje o czasie od startu, obecnymm numerze okrazenia i czasie od rozpoczecia okrazenia
 * Wysylana przez modul kierowcy jako odpowiedz na CAN_ID_TIME_START_ASK, oraz przy kazdym dodaniu okrazenia
 * Ramka typu DANE, dlugosc 5 bajt�w:
 * 1 bajt -> numer okrazenia
 * 2 bajt -> sekund od rozpoczecia okrazenia
 * 3 bajt -> minut od rozpoczecia okrazenia
 * 4 bajt -> sekund od startu
 * 5 bajt -> minut od startu
 * Przed startem numer okrazenia jest rowny 0. CZAS OD STARTU JAK I CZAS OKRAZENIA NIE MUSI BYC ROWY 0
 * Wysyla:
 * 	Modul kierowcy
 * Odbiera:
 *
 */

#define CAN_ID_TIME_LAP_ASK CAN_ID_TIME_LAP+1 //1435
/*
 * Pytanie o CAN_ID_TIME_START
 * Ramka typu RTR, wysylana przez modul zaninteresowany aktalnymi czasami okraze�, czasem od startu i numerem okrazenia
 * Modul kierowcy odpowiada ramka CAN_ID_TIME_START
 * Kto wysyla ramke RTR/pyta:
 * 
 * Odpowiada/Pytanie odbierane przez:
 * 	Modul kierowcy
 */


#define CAN_ID_ACCELERATION_SIGNAL 1440 //1440
/*
 * Zawiera informacje o poziomie wcisnieci pedalu gazu
 * Wysylana przez modul kierowcy co okreslny czas
 * Ramka typu DANE, dlugosc 5 bajt�w:
 * 1 bajt -> poziom wcisniecia pedalu gazu

 * Wysyla:
 * 	Modul kierowcy
 * Odbiera:
 * telemetria
 * modul sterownia silnikami (jezeli obecny)
 * modul akwizycji
*/

#define CAN_ID_START 1600
/*
 * Zawiera informacje o restarcie danego modulu
 * Wysylana przez kazdy z modul�w podczas startu
 * Ramka typu DANE, dlugosc 0 bajtow
 * Kazdy z modulow ma swoje wlasne ID z ktorego wysyla dane
 * ID nalezy nadawac po kolei
 * np.:
 * #define CAN_ID_START_MODULE_DRIVER CAN_ID_START+1
 * Wysyla:
 * Kazdy startujacy modul
 * Odbiera:
 * Debuger/tester
 */
///////////poczatek CAN_ID_START
#define CAN_ID_START_MODULE_DRIVER CAN_ID_START+1 //1601
#define CAN_ID_START_MODULE_LIGHTS CAN_ID_START+2 //1602
#define CAN_ID_START_MODULE_TELEMETRY CAN_ID_START+3 //1603
/////////koniec CAN_ID_START

#define CAN_ID_ERROR 300
/*
 * Zawiera informacje o bledzie krytycznym jednego z modulow
 * Wysylany przez modul w momecie nastapienia bledu
 * Ramka typu DANE, dlugosc 4 bajty
 * 1 bajt -> kod bledu 1
 * 2 bajt -> kod bledu 2
 * 3 bajt -> kod bledu 3
 * 4 bajt -> kod bledu 4
 * 5 bajt -> minut od startu
 *  Kazdy z modulow ma swoje wlasne ID z ktorego wysyla dane
 * ID nalezy nadawac po kolei
 *  np.:
 * #define CAN_ID_ERROR_MODULE_DRIVER CAN_ID_ERROR+1
 */
///////////poczatek CAN_ID_ERROR
#define CAN_ID_ERROR_MODULE_DRIVER CAN_ID_ERROR+1 //301
#define CAN_ID_ERROR_MODULE_LIGHTS CAN_ID_ERROR+2 //302
#define CAN_ID_ERROR_MODULE_TELEMETRY CAN_ID_ERROR+3 //303
/////////koniec CAN_ID_ERROR

//*************************************************************//
/*
 * Propozycja formatki
 *
 *
 * #define CAN_ID_ROZDZIAL_XXX
 * #define CAN_ID_ROZDZIAL_XXX_ASK
 *
 * - Opisujemy co zawiera ramka (og�lny opis)
 * - Opisujemy mniej wiecej zachowania, kto wysyla dla ramek DATA,
 *   lub kto odpowiada dla ramek typu RTR. Na jakie ID jest to odpowiedz. Kiedy ramka jest wysylana.
 * - Dla ramek wysylanych cyklicznie napisac co ile jest ona wysylana
 * - Opisac parametry ramki. Typ DATA/RTR, dlugosc.
 * - Opisac KAZDY BAJT
 * - Jezeli to konieczne zastosowac przyklad
 * - Uzupelnic dla ramek DATA
 * 		Wysyla:
 *
 * 		Odbierany przez:
 *
 *  Dla ramek RTR
 *  	Kto wysyla ramke RTR/pyta:
 *
 *  	Kto dobiera ramke RTR/odpowiada na nia
 */


#endif
