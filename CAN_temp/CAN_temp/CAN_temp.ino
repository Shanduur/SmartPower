#include <SPI.h>
#include <MCP2515.h>
#include "Can_ID.h"
#define CAN_CLK 8 //kwarc
//#define CAN_BAUD 250 //predkosc CAN
#define CAN_SPI_MCP_PIN	 4 //pin podlaczenia interfejsu spi
#define CAN_INTERRUPT_PIN 2 //podlaczenie interupta 
#define CAN_INTERRUPT_NUMBER 0 //numer interupta
#define CAN_RTR 1 //czy ramka z danymi czy remote
#define CAN_DATA 0
#define TIME_TIME 1000
MCP2515 CAN(CAN_SPI_MCP_PIN, CAN_INTERRUPT_PIN);
Frame message;
//uint8_t buffer;
//****************************prototypy funckji*************************//
void CAN_Interrupt(void); //przerwanie wywoływane przy przyjściu wiadomości
void CAN_InitStageOne(void); //pierwszy etap inicjalizacji CAN
void SPI_Init(void); //inicjalizacja SPI na potrzeby CAN
/////////////////////////////////////////////////////////////
unsigned long currentTime_time = millis();
void setup() {
	Serial.begin(115200);
	Serial.println("Rozpoczynam inicjalizacje");

	Serial.println("Inicjalizuje CAN");
	CAN_InitStageOne();

	//Serial.println("Inicjalizuje Przerwania");
	attachInterrupt(CAN_INTERRUPT_NUMBER, CAN_Interrupt, FALLING); // start interrupt // wlaczenie przrwan od modulu CAN

	Serial.println("Zakonczenie inicjalizacji");
	Serial.println("wysyłanie ramek kontrolnych");
	uint8_t test[8];
	test[0] = 1;
	test[1] = 2;
	test[2] = 3;
	delay(100);

	Serial.println("===================================================");
	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/*//tak wyglada wysylanie rtr
	message.rtr = CAN_RTR;
	message.id = CAN_ID_LIGHTS_ASK_COMMAND_PARKING;
	//CAN.LoadBuffer(buffer, &message);
	Serial.println(message.id);
	CAN.EnqueueTX(message);
	delay(100);
	/////////////////////////////////////////////////////////////////////////////
	message.data.byte[0] = frontLights_status;
	//Serial.println(frontLights_status);
	message.id = CAN_ID_LIGHTS_STATUS_PARKING;
	message.rtr = CAN_DATA;
	CAN.EnqueueTX(message);
	/////////////////////////////////////////////////////////////////////////////
	*/
}
void CAN_Interrupt(void)
{
	CAN.intHandler();
	if (CAN.GetRXFrame(message))
	{
		/*if (message.id == CAN_ID_LIGHTS_ASK_STATUS_PARKING)
		{
		}*/
		/*Serial.print("ID = ");
		Serial.print(message.id);

		Serial.print("\t");
		if (message.extended)
		{
		Serial.print("EXTENDED");
		}
		else
		{
		Serial.print("STANDARD");
		}

		Serial.print("\t");
		if (message.rtr)
		{
		Serial.println("REMOTE");
		}
		else
		{
		Serial.print("DATA");
		Serial.print("\t len:");
		Serial.println(message.length);
		for (int i = 0; i < message.length; i++)
		{
		Serial.print(message.data.byte[i], DEC);
		Serial.print("\t");
		}
		Serial.println();
		}
		Serial.println("===================================================");
		//CAN.EnqueueTX(message);*/
	}
}
void SPI_Init(void)
{
	// Set up SPI Communication
	// dataMode can be SPI_MODE0 or SPI_MODE3 only for MCP2515
	SPI.setClockDivider(SPI_CLOCK_DIV2);
	SPI.setDataMode(SPI_MODE0);
	SPI.setBitOrder(MSBFIRST);
	SPI.begin();
}
void CAN_InitStageOne(void)
{
	//int count = 50;                                     // the max numbers of initializint the CAN-BUS, if initialize failed first!.  
	//do {
	//	if (CAN_OK == CAN.begin(CAN_500KBPS, MCP_16MHz))             // init can bus : baudrate = 500k
	//	{
	//		Serial.println("CAN_InitStageOne - inicjalziacja powiodla sie");
	//		break;
	//	}
	//	else
	//	{
	//		Serial.println("CAN_InitStageOne - inicjalizacja modulu CAN nie powiodla sie");
	//		Serial.println("CAN_InitStageOne - ponowna proba inicjalizacji");
	//		delay(10);
	//		if (count <= 1)
	//		{
	//			Serial.println("CAN_InitStageOne - inicjalizacja modulu nie powiodla sie");
	//		}
	//		
	//	}

	//} while (count--);

	SPI_Init();
	if (CAN.Init(CAN_BAUDRATE, CAN_CLK))
	{
		Serial.println("MCP2515 Init OK ...");
	}
	else {
		Serial.println("MCP2515 Init Failed ...");
	}

	attachInterrupt(CAN_INTERRUPT_NUMBER, CAN_Interrupt, FALLING);
	CAN.InitFilters(false);
	CAN.SetRXMask(MASK0, 0x0, 0); //match all but bottom four bits
	CAN.SetRXFilter(FILTER0, 0x100, 0); //allows 0x100 - 0x10F
										//So, this code will only accept frames with ID of 0x100 - 0x10F. All other frames
										//will be ignored.
										//CAN.SetRXMask(MASK0, 0x0, 0); //match all but bottom four bits
										//CAN.SetRXFilter(FILTER1, 0x514, 0);//od 1300
}

void loop() {
	////////////////////////////////////////////////////
	if (millis() - currentTime_time >= TIME_TIME)
	{
		currentTime_time = millis();

		message.rtr = CAN_DATA;
		message.id = CAN_ID_CURRENT;
		message.data.byte[0] = 10;
		message.data.byte[0] = 30;
		Serial.println(message.id);
		CAN.EnqueueTX(message);
		delay(100);
	}
	/////////////////////////////////////////////////////
}
