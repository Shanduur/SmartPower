#ifndef _CAN_ID_h
#define _CAN_ID_h

//rozkazy
#define CAN_TURN_ON 2
#define CAN_TURN_OFF 0
#define CAN_TOGGLE 1

//statusy
#define CAN_STATUS_ON 3
#define CAN_STATUS_OFF 4
#define CAN_STATUS_UNKNOWN 5

#define CAN_BAUDRATE 250

//statusy startu modu�u
#define CAN_MODULE_START_POWER 1 //po zaniku zasilania
#define CAN_MODULE_START_RESET_PIN 2// przez przycisk reset
#define CAN_MODULE_START_WATCHDOG 3//przez watchdog
#define CAN_MODULE_START_SOFTWARE 4//przez oprogramowanie
#define CAN_MODULE_START_OTHER 5//inny

//CAN IDs

//*************************************o�wietlenie*************************************************************//
#define CAN_ID_LIGHTS 1300	//bazowe id dla oswietlenia zewnetrznego
/*
* Zawiera informacj� o aktualnych stanach �wiatel wysylana przez modu� kierowcy po restarcie
* Ramka typu DANE, d�ugo�c 6 bajt�w:
* 1 bajt -> stan �wiate� pozycyjnych
* 2 bajt -> stan �wiate� awaryjnych
* 3 bajt -> stan �wiate� stopu
* 4 bajt -> stan kierunkowskazu lewego
* 5 bajt -> stan kierunkowskazu prawego
* 6 bajt -> stan wycieraczek
* Wysy�a:
* Modul kierowcy
* Odbiera:
* modu� �wiate�
*/
#define CAN_ID_LIGHTS_BLIKERS CAN_ID_LIGHTS+20

//********�wiat�a pozycyjne*******//
#define CAN_ID_LIGHTS_COMMAND_PARKING CAN_ID_LIGHTS+1	//1301 //ID rozkazu w�/wy�/przel o�wietlenia zewn�trznego
/*
 Sk�ada si� z jednego bajtu
 CAN_TURN_ON - �wiat�a powinny zosta� w��czone
 CAN_TURN_OFF - �wiat�a powinny zosta� wy��czone
 CAN_TOGGLE - przelaczenie na stan przeciwny

 Wys�ana jako ramka z danymi - rozkaz w�/wy�/przel �wiate� pozycyjnych
 Odebranie jej przerz modu� steruj�cy o�wietleniem musi powodowac w�/wy�/przel �wiate� pozycyjnych.

 Wysy�a dane - modu� kierowcy
 Obiera dane - modu� steruj�cy o�wietleniem
 */

#define CAN_ID_LIGHTS_ASK_COMMAND_PARKING CAN_ID_LIGHTS+2	//1302	//pro�ba o ponowne przes�anie rozkazu wl/wyl/przel
/*
 Wys�ana jako ramka Remote Request - ��danie odes�ania ramki CAN_ID_LIGHTS_COMMAND_PARKING
 z aktualnym statusem �wiate� pozycyjnych.
 Odebranie jej przez modu� kt�ry ZNA aktualny stan �wiate� zewn�trznych musi powodowac
 odes�anie ramki CAN_ID_LIGHTS_COMMAND_LIGHTS.

 Wysy�a pytanie - modu� steruj�cy o�wietleniem np. po restarcie
 Odbiera pytanie - modu� ZNAJ�CY stan �wiate� pozycyjnych - modul kierowcy
 */

#define CAN_ID_LIGHTS_STATUS_PARKING CAN_ID_LIGHTS+3 //1303	//Pro�ba o stan �wiate� pozycyjnych
/*
 Ramka z danymi
 Sk�ada si� z jednego bajtu
 CAN_STATUS_ON	- �wiat�a w��czone
 CAN_STATUS_OFF	- �wiat�a wy��czone

 Ramka z danymi - powiadamia o tym, czy �wiat�a s� w��czone/wy��czone.
 Odebranie tej ramki jest potwierdzniem w��czenia lub nie �wiate� pozycyjnych.

 Zawsze zawiera informacj� o obecnym statusie �wiate� pozycyjnych.
 Jest to potwierdzeie dzia�ania �wiate� pozycyjnych.

 Wysy�a dane - modu� ZNAJ�CY stan �wiate� pozycyjnych - modul oswietlenia, modul oswietlenia przy kazdej zmianie statusu
 Obiera dane - ka�dy zainteresowany modu� - np. modul kierowcy
 */

#define CAN_ID_LIGHTS_ASK_STATUS_PARKING  CAN_ID_LIGHTS+4 //1304
/*
 Ramka typu Remote Request. Po odebraniu nale�y odes�ac ramke CAN_ID_LIGHTS_STATUS_PARKING
 (tylko  przez modu� kt�ry ZNA obecny status).


 Wysy�a pytanie - ka�dy zainteresowany modu� - np modul kierowcy
 Odbiera pytanie - modu� ZNAJ�CY stan �wiate� pozycyjnych -
 */

//********kierunkowskazy*******//
#define CAN_ID_LIGHTS_BLIKERS CAN_ID_LIGHTS+20//1320
//prawy kierunkowskaz
#define CAN_ID_LIGHTS_COMMAND_R_BLINKER CAN_ID_LIGHTS_BLIKERS+1 //1321
/*
 * Rozkaz wlaczenia prawego kierunkowskazu
 Sk�ada si� z jednego bajtu
 CAN_TURN_ON - prawy kierunkowskaz powinien zostac w��czony na 3 migniecia

 Wysy�a dane - modu� kierowcy
 Obiera dane - modu� steruj�cy o�wietleniem,

 */
#define CAN_ID_LIGHTS_COMMAND_L_BLINKER CAN_ID_LIGHTS_BLIKERS+2 //1322
/*
 * Rozka wlaczenia lewego kierunkowskazu
 Sk�ada si� z jednego bajtu
 CAN_TURN_ON - lewy kierunkowskaz powinien zostac w��czony na 3 migniecia

 Wysy�a dane - modu� kierowcy
 Obiera dane - modu� steruj�cy o�wietleniem,

 */
#define CAN_ID_LIGHTS_COMMAND_HAZARD_BLINKER CAN_ID_LIGHTS_BLIKERS+3 //1323
/*
 * Rozkaz wlaczenia kierunkowskazow awaryjnych
 * Ramka typu dane
 * Sk�ada si� z jednego bajtu
 * CAN_TURN_ON - wlaczenie kierunkowskazow awaryjnych
 * CAN_TURN_OFF - wylaczenie kierunkowskazow awaryjnych
 * CAN_TURN_TOGGLE - prze��czenie kierunkowskazow awaryjnych
 *
 * Wys�ana jako ramka z danymi - rozkaz w�/wy�/przel migania kierunkowskazow
 * Odebranie jej przez modu� steruj�cy o�wietleniem musi powodowac w�/wy�/przel migania kierunkowskaz�w.
 * Wysy�a dane - modu� kierowcy
 * Obiera dane - modu� steruj�cy o�wietleniem,
 */

#define CAN_ID_LIGHTS_ASK_COMMAND_HAZARD_BLINKER CAN_ID_LIGHTS_BLIKERS+4 //1324
/*
 * Wys�ana jako ramka Remote Request - ��danie odes�ania ramki CAN_ID_LIGHTS_COMMAND_HAZARD_BLINKER
 * z aktualnym poleceniem �wiate� awaryjnych
 * Odebranie jej przez modu� kt�ry ZNA aktualny stan kierunkowskazu musi powodowac
 * odes�anie ramki CAN_ID_LIGHTS_COMMAND_R_BLINKER.
 *
 * Wysy�a pytanie - modu� steruj�cy o�wietleniem
 * Odbiera pytanie - modu� ZNAJ�CY stan kierunkowskaz�w	- modul kierowcy
 */

#define CAN_ID_LIGHTS_ASK_STATUS_HAZARD_BLINKER CAN_ID_LIGHTS_BLIKERS+5 //1325
/*
 * Pytanie o status �wiatel awaryjnych
 * Przesy�ane jako ramka Remote Request
 * Odebranie jej przez modul oswietlenia musi powodowac odeslanie aktualnego statusu
 * swiatel awaryjnych CAN_ID_LIGHTS_STATUS_HAZARD_BLINKER
 *
 * Wysyla pytanie - modul kierowcy/inny zainteresowany
 * Odbiera pytanie - modul oswietlenia
 */

#define CAN_ID_LIGHTS_STATUS_HAZARD_BLINKER CAN_ID_LIGHTS_BLIKERS+6 //1326
/*
 * Ramka z danymi o obecnym statusie swiatel awaryjnych
 * Przesy�ane jako ramka z dannymi
 * Wysylana kazdorazowo przy zmanie statusu swiatel awaryjnych
 *
 *
 * Wysyla dane - modul oswietlenia
 * Odbiera dane - modul kierowcy
 */

//*********inne wejscia***********//
#define CAN_ID_BRAKE_STATUS 1000
/*
 * Ramka z danymi wysylana przy kazdej zmianie stanu czujnika hamulca lub jako odpowiedz na CAN_ID_BRAKE_ASK_STATUS
 * Zawiera jeden bajt
 * CAN_TURN_ON - hamulec nozny wcisniety
 * CAN_TURN_OFF - hamulec nozny NIE wcisniety
 * Wysyla dane - modul kierowcy
 */

#define CAN_ID_BRAKE_ASK_STATUS CAN_ID_BRAKE_STATUS+1 //1001
/*
 * Ramka wysylana jako RTR, jej odebranie przez modul kierowcy musi powodowac odeslanie CAN_ID_BRAKE_STATUS
 * Wysyla pytanie - kazdy zaintereesowany - modul oswietlenia
 * Odbiera pytanie - modul kierowcy
 */

#define CAN_ID_EMERGENCY_SWITCH_STATUS 1002  //1002
/*
 * Ramka z danymi wysylana przy kazdej zmianie stanu przycisku bezpieczenstwa lub jako odpowiedz na CAN_ID_EMERGENCY_SWITCH_ASK_STATUS
 * Zawiera jeden bajt
 * CAN_TURN_ON - przycisk wcisniety
 * CAN_TURN_OFF - przycisk NIE wcisniety
 * Wysyla dane - modul kierowcy
 */

#define CAN_ID_EMERGENCY_SWITCH_ASK_STATUS CAN_ID_EMERGENCY_SWITCH_STATUS+1 //1003
/*
 * Ramka wysylana jako RTR, jej odebranie przez modul kierowcy musi powodowac odeslanie CAN_ID_EMERGENCY_SWITCH_STATUS
 * Wysyla pytanie - kazdy zaintereesowany
 * Odbiera pytanie - modul kierowcy
 */

#define CAN_ID_WIPER_SWITCH_STATUS 1100
/*
 * Ramka z danymi wysylana przy kazdej zmianie stanu prze��cznika wycieraczek lub jako odpowiedz na CAN_ID_WIPER_SWITCH_ASK_STATUS
 * Zawiera jeden bajt
 * CAN_TURN_ON - wycieraczka ma byc w��czona
 * CAN_TURN_OFF - wycieraczka ma byc wylaczona
 * Wysyla dane - modul kierowcy
 */

#define CAN_ID_WIPER_SWITCH_ASK_STATUS CAN_ID_WIPER_SWITCH_STATUS+1 //1101
/*
 * Ramka wysylana jako RTR, jej odebranie przez modul kierowcy musi powodowac odeslanie CAN_ID_WIPER_SWITCH_STATUS
 * Wysyla pytanie - kazdy zaintereesowany - modul oswietlenia
 * Odbiera pytanie - modul kierowcy
 */

/*
 * **************************************DANE****************************************
 */
#define CAN_ID_DATA 1400 //bazowe id dla danych "analogowych" takich jak predkosci obrotowe, prady napiecia,

//*************************************prad i napiecie ogniwa*************************************************************//
#define CAN_ID_CURRENT CAN_ID_DATA+1    //1401 //bazowe id dla pradu ogniwa
/*
 * Zawiera informacj� o pradzie w ukladzie ogniwa wodorowego (prad wyjsciowy)
 * Wysy�ana przez modu� pomiarowy co okreslny czas
 * Ramka typu DANE, d�ugo�c 2 bajt�w:
 * 1 bajt -> prad czesc calkowita
 * 2 bajt -> prad czesc u�amkowa (wartosc 1 = 0.01A)
 * Wysy�a:
 * Modul pomiarow
 * Odbiera:
 * telemetria
 * modul kierowcy
 * modu� akwizycji
 */

#define CAN_ID_VOLTAGE CAN_ID_DATA+5    //1405//bazowe id dla napiecia ogniwa
/*
 * Zawiera informacj� o napieciu w ukladzie ogniwa wodorowego (napiecie wyjsciowe)
 * Wysy�ana przez modu� pomiarowy co okreslny czas
 * Ramka typu DANE, d�ugo�c 2 bajt�w:
 * 1 bajt -> napiecie czesc calkowita
 * 2 bajt -> prad czesc u�amkowa (wartosc 1 = 0.01V)
 * Wysy�a:
 * Modul pomiarow
 * Odbiera:
 * telemetria
 * modul kierowcy
 * modu� akwizycji
 */

//*************************************temperatura ogniwa*************************************************************//
#define CAN_ID_CELL_TEMPERATURE CAN_ID_DATA + 10 //1410    //bazowe id dla temperatury ogniwa
/*
 * Zawiera informacj� o temperaturze ogniwa wodorowego
 * Wysy�ana przez modu� kontorli wentylatorow co okreslny czas
 * Ramka typu DANE, d�ugo�c 4 bajt�w:
 * 1 bajt -> temperatura czuknika 1 calkowita
 * 2 bajt -> temperatura czuknika 1 (wartosc 1 = 0.01)
 * 3 bajt -> temperatura czuknika 2 calkowita
 * 4 bajt -> temperatura czuknika 2 (wartosc 1 = 0.01)
 * Wysy�a:
 * Kontroler wentylatorow
 * Odbiera:
 * telemetria
 * modul kierowcy
 * modu� akwizycji
 */

//*************************************predkosc bolidu*************************************************************//
#define CAN_ID_CAR_VELOCITY CAN_ID_DATA + 15	//1415    //bazowe id dla predkosci
/*
 * Zawiera informacj� o predkosci pojazu (w km/h)
 * Wysy�ana przez modu� kontroli wentylatorow co okreslny czas
 * Ramka typu DANE, d�ugo�c 2 bajt�w:
 * 1 bajt -> predkosc czesc calkowita
 * 2 bajt -> predkosc czesc u�amkowa (wartosc 1 = 0.01km/h)
 * Wysy�a:
 * Kontroler wentylatorow
 * Odbiera:
 * telemetria
 * modul kierowcy
 * modu� akwizycji
 */

#define CAN_ID_ACCELERATION_SIGNAL CAN_ID_DATA + 20 //1420
/*
 * Zawiera informacj� o poziomie wci�ni�ci peda�u gazu
 * Wysy�ana przez modu� kierowcy co okreslny czas
 * Ramka typu DANE, d�ugo�c 5 bajt�w:
 * 1 bajt -> poziom wcisniecia pedalu gazu

 * Wysy�a:
 * Modu� kierowcy
 * Odbiera:
 * telemetria
 * modul sterownia silnikami (je�eli obecny)
 * modu� akwizycji
 */

//*************************************czas i numer okrazenia*************************************************************//
#define CAN_ID_TIME 1430
/*
 * Zawiera informacj� o obecnej godzinie i dacie .
 * Wysy�ana przez modu� kierowcy tylko jako odpowiedz na pro�b�
 * Tzn. Po odebraniu zapytania CAN_ID_TIME_ASK
 * Ramka typu DATA, d�ugo�c 6 bajt�w
 * bajt 1 -> sekunda
 * bajt 2 -> minuta
 * bajt 3 -> godzina
 * bajt 4 -> dzie�
 * bajt 5 -> miesiac
 * bajt 6 -> rok
 * Wysy�a:
 * 	Modu� kierowcy
 * Odbierany przez:
 */

#define CAN_ID_TIME_ASK CAN_ID_TIME+1	//1431
/*
 * Pytanie o CAN_ID_TIME
 * Ramka typu RTR, wysy�ana przez modu� zaninteresowany aktualn� dat�
 * Modul kierowcy odpowiada ramka CAN_ID_TIME
 * Kto wysyla ramke RTR/pyta:
 *
 * Odpowiada/Pytanie odbierane przez:
 * 	Modu� kierowcy
 */

#define CAN_ID_TIME_SYNC CAN_ID_TIME_ASK+1	//1432
/*
 * Zawiera informacj� o obecnej godzinie i dacie .
 * Wysy�ana przez modu� kierowcy tylko na pro�b� i tylko w mom�cie dodania kolejnej sekundy
 * Tzn. Po odebraniu zapytania CAN_ID_TIME_SYNC_ASK modu� kierowcy czeka na moment pe�nej sekundy i odsy�a t� ramk�
 * Ramka typu DATA, d�ugo�c 6 bajt�w
 * bajt 1 -> sekunda
 * bajt 2 -> minuta
 * bajt 3 -> godzina
 * bajt 4 -> dzie�
 * bajt 5 -> miesiac
 * bajt 6 -> rok
 * Wysy�a:
 * Modu� kierowcy
 * Odbieramy przez:
 *
 */

#define CAN_ID_TIME_SYNC_ASK CAN_ID_TIME_SYNC +1 //1433
/*
 * Pytanie o CAN_ID_TIME_SYNC
 * Ramka typu RTR, wysy�ana przez modu� zaninteresowany aktualn� dat� oraz synchronizacja czasu
 * Modul kierowcy odpowiada ramka CAN_ID_TIME_SYNC
 * Kto wysyla ramke RTR/pyta:
 *
 * Odpowiada/Pytanie odbierane przez:
 * 	Modu� kierowcy
 */

#define CAN_ID_TIME_LAP CAN_ID_TIME_SYNC_ASK +1 //1434
/*
 * Zawiera informacj� o czasie od startu, obecnymm numerze okr��enia i czasie od rozpocz�cia okr��enia
 * Wysy�ana przez modu� kierowcy jako odpowiedz na CAN_ID_TIME_START_ASK, oraz przy kazdym dodaniu okrazenia
 * Ramka typu DANE, d�ugo�c 5 bajt�w:
 * 1 bajt -> numer okr��enia
 * 2 bajt -> sekund od rozpocz�cia okr��enia
 * 3 bajt -> minut od rozpocz�cia okr��enia
 * 4 bajt -> sekund od startu
 * 5 bajt -> minut od startu
 * Przed startem numer okrazenia jest rowny 0. CZAS OD STARTU JAK I CZAS OKRAZENIA NIE MUSI BYC ROWY 0
 * Wysy�a:
 * 	Modu� kierowcy
 * Odbiera:
 *
 */

#define CAN_ID_TIME_LAP_ASK CAN_ID_TIME_LAP+1 //1435
/*
 * Pytanie o CAN_ID_TIME_START
 * Ramka typu RTR, wysy�ana przez modu� zaninteresowany aktalnymi czasami okr�ze�, czasem od startu i numerem orazenia
 * Modul kierowcy odpowiada ramka CAN_ID_TIME_STAR
 * Kto wysyla ramke RTR/pyta:
 *
 * Odpowiada/Pytanie odbierane przez:
 * 	Modu� kierowcy
 */

#define CAN_ID_START 1600
/*
 * Zawiera informacj� o restarcie danego modulu
 * Wysy�ana przez ka�dy z modu��w podczas startu
 * Ramka typu DANE, d�ugosc 0 bajtow
 * Ka�dy z modulow ma swoje wlasne ID z ktorego wysyla dane
 * ID nalezy nadawac po kolei
 * np.:
 * #define CAN_ID_START_MODULE_DRIVER CAN_ID_START+1
 * Wysyla:
 * Kazdy startujacy modul
 * Odbiera:
 * Debuger/tester
 * - modu� karty SD
 */
///////////poczatek CAN_ID_START
#define CAN_ID_START_MODULE_DRIVER CAN_ID_START+1	//1601
#define CAN_ID_START_MODULE_SD CAN_ID_START+2		//1602
#define CAN_ID_START_MODULE_LIGHTS CAN_ID_START+3		//1603
#define CAN_ID_START_MODULE_TELEMETRY CAN_ID_START+4		//1604
/////////koniec CAN_ID_START

#define CAN_ID_ERROR 300
/*
 * Zawiera informacje o bledzie krytycznym jednego z modulow
 * Wysylany przez modul w momecie nastapienia bledu
 * Ramka typu DANE, d�ugosc 4 bajty
 * 1 bajt -> kod bledu 1
 * 2 bajt -> kod bledu 2
 * 3 bajt -> kod bledu 3
 * 4 bajt -> kod bledu 4
 * 5 bajt -> minut od startu
 *  Ka�dy z modulow ma swoje wlasne ID z ktorego wysyla dane
 * ID nalezy nadawac po kolei
 *  np.:
 * #define CAN_ID_ERROR_MODULE_DRIVER CAN_ID_ERROR+1
 */
///////////poczatek CAN_ID_ERROR
#define CAN_ID_ERROR_MODULE_DRIVER CAN_ID_ERROR+1	//301
#define CAN_ID_ERROR_MODULE_SD CAN_ID_ERROR+2	//302
#define CAN_ID_ERROR_MODULE_LIGHTS CAN_ID_ERROR+3	//301
#define CAN_ID_ERROR_MODULE_TELEMETRY CAN_ID_ERROR+4	//301
/////////koniec CAN_ID_ERROR

//*************************************************************//
/*
 * Propozycja formatki
 *
 *
 * #define CAN_ID_ROZDZIAL_XXX
 * #define CAN_ID_ROZDZIAL_XXX_ASK
 *
 * - Opisujemy co zawiera ramka (og�lny opis)
 * - Opisujemy mniej wi�cej zachowania, kto wysy�a dla ramek DATA,
 *   lub kto odpowiada dla ramek typu RTR. Na jakie ID jest to odpowiedz. Kiedy ramka jest wysy�ana.
 * - Dla ramek wysy�anych cyklicznie napisac co ile jest ona wysy�ana
 * - Opisac parametry ramki. Typ DATA/RTR, dlugo�c.
 * - Opisac KAZDY BAJT
 * - Jezeli to konieczne zastosowac przyklad
 * - Uzupe�nic dla ramek DATA
 * 		Wysy�a:
 *
 * 		Odbierany przez:
 *
 *  Dla ramek RTR
 *  	Kto wysyla ramke RTR/pyta:
 *
 *  	Kto dobiera ramk� RTR/odpowiada na nia
 */

#endif
