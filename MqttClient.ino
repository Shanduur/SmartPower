//////////////////////////////// UWAGI ///////////////////////////////////////
//Nalezy zastanowic sie nad przeniesiem scalania danych odebranych w dane wysylane gdzies indziej niz funkcja przerwania
//Nie wiem czy potrzebna jest taka dokladnosc dla czasu okrazenia i startu az do milisec i w czasie okrazenia chyba nie potrzeba godzin.
//Jezeli chcemy miec czas w godzinach i milisek to trzeba to zmienic tez w wysyłaniu i Can_ID
//Pytanie na ile dokladne wyniki otrzymamy pradu, napiecia i temperatury (czy potrzebny jest double czy float wystarczy?)
//////////////////////////////////////////////////////////////////////////////
/*
 * Subskrypcja wiadomosci w mosquitto-clients (sudo apt-get install mosquitto-clients:
 * 
 * Debug:
 *   mosquitto_sub -h test.mosquitto.org -t SmartPower/debug -q -l 
 *   
 * Dane telemetryczne:
 * 
    mosquitto_sub -h test.mosquitto.org -t SmartPower/prad -t SmartPower/temperatura 
    -t SmartPower/okrazenie -t SmartPower/czasOkrazenia -t SmartPower/dlugosc 
    -t SmartPower/szerokosc -q -l
 *
 */

#define TINY_GSM_MODEM_SIM900
/*
 * Definicja modulu ktory jest aktualnie uzywany 
 * 
 */

#include <TinyGsmClient.h>
/*
 * Biblioteka zawierajaca obsluge MQTT
 * 
 */
#include <PubSubClient.h>
/*
 * Publikacja i Subskrypcja
 * 
 */
 
 #include "GetGPS.h"
 /*
  * Definicje funkcji zwiazanych z GPSem
  *
  */

#define SerialMon Serial
/* 
 * Ustawienie serialu dla debugu w konsoli (dla Serial Monitora, bazowa predkosc to 115200) 
 * 
 */

#define SerialAT Serial1
/* 
 *  Hardware Serial uzywany na Mega, Leonardo, Micro
 *
 */
// ******* CAN *******
#define TEMP_TIME 10000
#define TIME_TIME 10000
#include <SPI.h>
#include <MCP2515.h>
#include "Can_ID.h"
#define CAN_CLK 8 //kwarc
//#define CAN_BAUD 250 //predkosc CAN
#define CAN_SPI_MCP_PIN  22 //pin podlaczenia interfejsu spi
#define CAN_INTERRUPT_PIN 2 //podlaczenie interupta 
#define CAN_INTERRUPT_NUMBER 0 //numer interupta
#define CAN_RTR 1 //czy ramka z danymi czy remote
#define CAN_DATA 0
MCP2515 CAN(CAN_SPI_MCP_PIN, CAN_INTERRUPT_PIN);
Frame message;


//****************************prototypy funckji*************************//
void CAN_Interrupt(void); //przerwanie wywo³ywane przy przyjœciu wiadomoœci
void CAN_InitStageOne(void); //pierwszy etap inicjalizacji CAN
void SPI_Init(void); //inicjalizacja SPI na potrzeby CAN
//--------------------------------------------------------------------------------------------------------------

const char apn[]  = "internet"; //T-Mobile
const char user[] = "";
const char pass[] = "";
/*
 * Dane APN karty SIM
 * Pozostawić puste, jesli user albo password sa nieuzywane
 * 
 */
 
// ******* Detale MQTT *******
const char* broker = "test.mosquitto.org";
/*
 * Adres brokera MQTT
 * test.mosquitto.org jest publiczny
 * 
 */
const char* topicDebug = "SmartPower/debug";

// ******* Definicje topiców z poszczególnymi parametrami *******
const char* topicPrad      	  	= "SmartPower/prad";
const char* topicNapiecie		= "SmartPower/napiecie";
const char* topicTemperatura    = "SmartPower/temperatura";
const char* topicOkrazenie      = "SmartPower/okrazenie";
const char* topicCzasOkrazenia  = "SmartPower/czasOkrazenia";
const char* topicCzasStart		= "SmartPower/czasStart";
const char* topicDlugosc     	= "SmartPower/dlugosc";
const char* topicSzerokosc      = "SmartPower/szerokosc";
const char* topicGPS            = "SmartPower/GPS";

// ******* Definicje zmiennych poszczególnych parametrów *******
//Czy potrzebna dokladnosc doubla na wielkosci pradu, temperatury itp?
//////////////////////////////////////
//PRAD
//zmienne do wysylania
float prad = 0.0;
//zmienne do odebrania
int prad_calk = 0;
int prad_dze = 0;
//////////////////////////////////////
//NAPIECIE
//zmienne do wysylania
float napiecie = 0.0;
//zmienne do odebrania
int nap_calk = 0;
int nap_dze = 0;
//////////////////////////////////////
//TEMPERATURA
uint8_t currentTime_temp = millis();//pobranie czasu poczatkowego do wysylania zadania aktualizacji temperatury
//zmienne do wysylania
float temperatura = 0.0;
//zmienne do odebrania
int temp_calk = 0;
int temp_dze = 0;
//////////////////////////////////////
//////////////////////////////////////
//PREDKOSC
//zmienne do wysylania
float predkosc = 0.0;
//zmienne do odebrania
int pred_calk = 0;
int pred_dze = 0;
//////////////////////////////////////
//CZAS
uint8_t currentTime_time = millis(); //pobranie czasu poczatkowego do wysylania zadania aktualizacji czasu 
int okrazenie = 3; 
struct czas_okrazenia
{
	int co_godziny;
	int co_minuty;
	int co_sekundy;
	int co_milisek;
} czas_okrazenia,czas_start;

czas_okrazenia.co_godziny	= 41; 
czas_okrazenia.co_minuty	= 42;
czas_okrazenia.co_sekundy	= 43;
czas_okrazenia.co_milisek	= 44;
//Pytanie czy potrzebny jest nam dokladnosc w czasie az do milisec?
czas_start.co_godziny	= 45; 
czas_start.co_minuty	= 46;
czas_start.co_sekundy	= 47;
czas_start.co_milisek	= 48;
//////////////////////////////////////
//GPS
char latitude[15]  	= "latitude";
char longitude[15]	= "longitude";
char altitude[6]  	= "altitude";
char date[16] 	  	= "date";
char time_g[7] 	    	= "time";
char satellites[3] 	= "satellites";
char speedOTG[10] 	= "speedOTG";
char course[10]   	= "course";
//////////////////////////////////////
/*
 * Wartosci przypisane do testow
 * 
 */

int modemGPS_licznik = 1;

TinyGsm modem(SerialAT);
TinyGsmClient client(modem);
PubSubClient mqtt(client);

long lastReconnectAttempt = 0;



// ****************************** SETUP ******************************
void setup() 
{
  // Ustawienie predkosci konsoli
  SerialMon.begin(115200);
  delay(100);

// ********************** MODEM **********************
  SerialAT.begin(9600);
  delay(100);
  /*
   * Ustawienie predkosci modulu GSM
   * Sprawdzanie domyslnej za pomoca sketchu AT_Debug
   * Dla modułu SIM900 IComSat 1.1 predkosc to 9600
   * 
   */

  // Restart trwa dosyc długo
  // Aby pominac, wywolaj init() zamiast restart()
  SerialMon.println("Inicjalizuje Modem...");
  modem.restart();

  String modemInfo = modem.getModemInfo();
  SerialMon.print("Modem: ");
  SerialMon.println(modemInfo);

  // Odblokuj karte SIM za pomoca kodu PIN jesli zablokowana
  //modem.simUnlock("1234");

  SerialMon.print("Oczekiwanie na siec...");
  if (!modem.waitForNetwork()) 
  {
    SerialMon.println(" blad");
    while (true);
  }
  SerialMon.println(" OK");

  SerialMon.print("Laczenie do ");
  SerialMon.print(apn);

  if (!modem.gprsConnect(apn, user, pass)) 
  {
    SerialMon.println(" blad");
    while (true);
  }
  SerialMon.println(" OK");

  // MQTT Broker setup
  mqtt.setServer(broker, 1883);
  mqtt.setCallback(mqttCallback);
}

boolean mqttConnect() 
{
  SerialMon.print("Laczenie z ");
  SerialMon.print(broker);
  if (!mqtt.connect("SmartPower MQTT Mosquitto test")) 
  {
    SerialMon.println(" blad");
    return false;
  }
  SerialMon.println(" OK");
  mqtt.publish(topicDebug, "modul telemetri SmartPower uruchomiony");
  return mqtt.connected();
}
/////////////////////////////////////////////////////////////////////////////////
//funckja do skladania zmiennych odebranych w calosc do wyslania
float assemble(int cz_calk, int cz_dze)
{
	float liczba = (float)(cz_dze / 100);
	liczba = liczba + cz_calk;
	return liczba;
}
/////////////////////////////////////////////////////////////////////////////////

// ****************************** SETUP ******************************
void loop()
{
	/////////////////////////////////////////////////////
	//wyslanie zapytania o temperature co zadany czas
	if (millis() - currentTime_temp >= TEMP_TIME)
	{
		currentTime_temp = millis();

		message.rtr = CAN_RTR;
		message.id = CAN_ID_SEND_CELL_TEMPERATURE;
		//CAN.LoadBuffer(buffer, &message);
		SerialMon.println(message.id);
		CAN.EnqueueTX(message);
		delay(100);
	}
	/////////////////////////////////////////////////////
	//wyslanie zapytania o czas co zadany czas
	if (millis() - currentTime_time >= TEMP_TIME)
	{
		currentTime_time = millis();

		message.rtr = CAN_RTR;
		message.id = CAN_ID_TIME_SYNC_ASK;
		//CAN.LoadBuffer(buffer, &message);
		SerialMon.println(message.id);
		CAN.EnqueueTX(message);
		delay(100);
	}
	/////////////////////////////////////////////////////
  if (mqtt.connected()) 
  {
    mqtt.loop();
  } 
  else 
  {
    // Reconnect every 10 seconds
    unsigned long t = millis();
    if (t - lastReconnectAttempt > 10000L) 
    {
      lastReconnectAttempt = t;
      if (mqttConnect()) 
      {
        lastReconnectAttempt = 0;
      }
    }
  }
}

void mqttCallback() 
{
  //Wysyłanie danych pomiarów i liczby okrazen
  mqtt.publish(topicPrad, prad);
  mqtt.publish(topicNapiecie, napiecie);
  mqtt.publish(topicTemperatura, temperatura);
  mqtt.publish(topicOkrazenie, okrazenie);
  //Wysyłanie czasu okrazenia
  mqtt.publish(topicCzasOkrazenia, czas_okrazenia.co_godziny);
  mqtt.publish(topicCzasOkrazenia, czas_okrazenia.co_minuty);
  mqtt.publish(topicCzasOkrazenia, czas_okrazenia.co_sekundy);
  mqtt.publish(topicCzasOkrazenia, czas_okrazenia.co_milisek);
  //Wysyłanie czasu od startu
  mqtt.publish(topicCzasOkrazenia, czas_start.co_godziny);
  mqtt.publish(topicCzasOkrazenia, czas_start.co_minuty);
  mqtt.publish(topicCzasOkrazenia, czas_start.co_sekundy);
  mqtt.publish(topicCzasOkrazenia, czas_start.co_milisek);
  //Wysyłanie wsp. GPS
  mqtt.publish(topicDlugosc, longitude);
  mqtt.publish(topicSzerokosc, latitude);
  mqtt.publish(topicGPS, altitude);
  mqtt.publish(topicGPS, date);
  mqtt.publish(topicGPS, time_g);
  mqtt.publish(topicGPS, satellites);
  mqtt.publish(topicGPS, speedOTG);
  mqtt.publish(topicGPS, course);

  SerialMon.print(" Wyslano zestaw danych:");
  SerialMon.print(modemGPS_licznik);
  modemGPS_licznik = modemGPS_licznik + 1;
}
void CAN_Interrupt(void)
{
	CAN.intHandler();
	if (CAN.GetRXFrame(message))
	{
		/////////////////////////////////////////////////
		//dane o pradzie
		if (message.id == CAN_ID_CELL_CURRENT)
		{
			SerialMon.println("Prad ID:");
			SerialMon.println(message.id);

			prad_dze = message.data.byte[0];
			prad_calk = message.data.byte[1];
			prad=assemble(prad_calk, prad_dze);

		}
		/////////////////////////////////////////////////
		//dane o napieciu
		if (message.id == CAN_ID_CELL_VOLTAGE)
		{
			SerialMon.println("Napiecie ID:");
			SerialMon.println(message.id);

			nap_dze = message.data.byte[0];
			nap_calk = message.data.byte[1];
			napiecie = assemble(nap_calk, nap_dze);
		}
		/////////////////////////////////////////////////
		//dane o predkosci
		if (message.id == CAN_ID_CAR_VELOCITY)
		{
			SerialMon.println("Predkosc ID:");
			SerialMon.println(message.id);

			pred_dze = message.data.byte[0];
			pred_calk = message.data.byte[1];
			predkosc = assemble(pred_calk, pred_dze);
		}
		/////////////////////////////////////////////////
		//dane o temperaturze
		if (message.id == CAN_ID_CELL_TEMPERATURE)
		{
			SerialMon.println("Temperatura ID:");
			SerialMon.println(message.id);

			temp_dze = message.data.byte[0];
			temp_calk = message.data.byte[1];
			temperatura = assemble(temp_calk, temp_dze);
		}
		/////////////////////////////////////////////////
		//dane o czasie
		if (message.id == CAN_ID_TIME_LAP)
		{
			SerialMon.println("Czas ID:");
			SerialMon.println(message.id);

			okrazenie = message.data.byte[0];
			czas_start.co_sekundy = message.data.byte[1];
			czas_start.co_minuty = message.data.byte[2];
			czas_okrazenia.co_sekundy = message.data.byte[3];
			czas_okrazenia.co_minuty = message.data.byte[4];
			
		}
		/////////////////////////////////////////////////
		/*SerialMon.print("ID = ");
		SerialMon.print(message.id);

		SerialMon.print("\t");
		if (message.extended)
		{
		SerialMon.print("EXTENDED");
		}
		else
		{
		SerialMon.print("STANDARD");
		}

		SerialMon.print("\t");
		if (message.rtr)
		{
		SerialMon.println("REMOTE");
		}
		else
		{
		SerialMon.print("DATA");
		SerialMon.print("\t len:");
		SerialMon.println(message.length);
		for (int i = 0; i < message.length; i++)
		{
		SerialMon.print(message.data.byte[i], DEC);
		SerialMon.print("\t");
		}
		SerialMon.println();
		}
		SerialMon.println("===================================================");
		CAN.EnqueueTX(message);*/
	}
}
void SPI_Init(void)
{
	// Set up SPI Communication
	// dataMode can be SPI_MODE0 or SPI_MODE3 only for MCP2515
	SPI.setClockDivider(SPI_CLOCK_DIV2);
	SPI.setDataMode(SPI_MODE0);
	SPI.setBitOrder(MSBFIRST);
	SPI.begin();
}
void CAN_InitStageOne(void)
{
	/*int count = 50;                                     // the max numbers of initializint the CAN-BUS, if initialize failed first!.
	do
	{
	if (CAN_OK == CAN.begin(CAN_500KBPS, MCP_16MHz))             // init can bus : baudrate = 500k
	{
	SerialMon.println("CAN_InitStageOne - inicjalziacja powiodla sie");
	break;
	}
	else
	{
	SerialMon.println("CAN_InitStageOne - inicjalizacja modulu CAN nie powiodla sie");
	SerialMon.println("CAN_InitStageOne - ponowna proba inicjalizacji");
	delay(10);
	if (count <= 1)
	{
	SerialMon.println("CAN_InitStageOne - inicjalizacja modulu nie powiodla sie");
	}

	}

	} while (count--); */

	SPI_Init();
	if (CAN.Init(CAN_BAUDRATE, CAN_CLK))
	{
		SerialMon.println("MCP2515 Init OK ...");
	}
	else {
		SerialMon.println("MCP2515 Init Failed ...");
	}

	attachInterrupt(CAN_INTERRUPT_NUMBER, CAN_Interrupt, FALLING);
	CAN.InitFilters(false);
	CAN.SetRXMask(MASK0, 0x0, 0); //match all but bottom four bits
	CAN.SetRXFilter(FILTER0, 0x100, 0);
	/*
	* allows 0x100 - 0x10F
	* So, this code will only accept frames with ID of 0x100 - 0x10F. All other frames
	* will be ignored.
	* CAN.SetRXMask(MASK0, 0x0, 0); //match all but bottom four bits
	* CAN.SetRXFilter(FILTER1, 0x514, 0);//od 1300
	*/
}

